<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EchoExtraFootBehavior
 *
 * @author 志鹏
 */
class EchoExtraFootBehavior extends Behavior {
    
    protected $options = array();
    
    public function run(&$params) {
        if(is_array($params)) {
            $params = implode("", $params);
        }
        return $params;
    }
    
}
