<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BaseDesktop
 *
 * @author 志鹏
 */
class BaseDesktop {
    
    protected $limit = 5;
    
    protected $view;
    
    protected $block_title;
    
    protected $link;
    
    protected $data;
    
    protected $options;
    
    public function __construct($options) {
        $this->options = $options;
        $this->limit = $this->options["listrows"] ? $this->options["listrows"] : $this->limit;
        $this->view = Think::instance("View");
    }
    
    public function after_run() {
        $enabledDesktop = C("ENABLED_DESKTOP");
        $this->view->assign("block_title", $enabledDesktop[$this->options["block_name"]]["label"]);
        $this->view->assign("link", $enabledDesktop[$this->options["block_name"]]["link"]);
        $this->view->assign("data", $this->data);
        return $this->view->fetch("../Desktop/".$this->options["block_name"]);
    }
    
}
