<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Desktop
 *
 * @author 志鹏
 */
class Desktop {
    
    public function getItems() {
        $model = D("UserDesktop");
        $desktops = $model->where("uid=".getCurrentUid())->order("position ASC, listorder DESC")->select();
        if(!$desktops) {
            return;
        }
        
        import("@.Desktop.BaseDesktop");
        foreach($desktops as $dt) {
            import("@.Desktop.blocks.Desktop".$dt["block_name"]);
            $className = "Desktop".$dt["block_name"];
            if(!class_exists($className)) {
                continue;
            }
            $block = new $className($dt);
            $block->run();
            $blockHTML[$dt["position"]][] = $block->after_run();
        }
        
        return $blockHTML;
        
    }
    
}
