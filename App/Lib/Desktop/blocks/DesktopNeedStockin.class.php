<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DesktopNeedStockin
 *
 * @author 志鹏
 */
class DesktopNeedStockin extends BaseDesktop {
    
    public function run() {
        import("@.Workflow.Workflow");
        $workflow = new Workflow("Stockin");
        $ids = $workflow->getRowIdsByNode("StartProcess");
        
        $stockin = D("Stockin");
        $map = array(
            "id" => array("IN", $ids)
        );
        $papers = $stockin->where($map)->limit($this->limit)->select();
        $this->data = $papers;
    }
    
}
