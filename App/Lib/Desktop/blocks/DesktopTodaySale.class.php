<?php

/**
 * @filename DesktopTodaySale.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-27  9:30:14
 * @Description
 * 
 */
class DesktopTodaySale extends BaseDesktop {
    
    public function run() {
        $order = D("Orders");
        $starttime = strtotime(date("Ymd", CTS));
        $endtime = $starttime + 3600*24;
        $map = array(
            "dateline" => array("BETWEEN", array($starttime, $endtime))
        );
        $order->includeWorkflowProcess = false;
        $tmp = $order->field("id")->where($map)->select();
        foreach($tmp as $t) {
            $ids[] = $t["id"];
        }
        
        if(!$ids) {
            return;
        }
        
        $map = array(
            "order_id" => array("IN", implode(",", $ids))
        );
        $orderDetail = D("OrdersDetailView");
        $this->data = $orderDetail->where($map)->limit($this->limit)->select();
    }
    
}

?>
