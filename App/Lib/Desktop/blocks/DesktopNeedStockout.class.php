<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DesktopNeedStockout
 *
 * @author 志鹏
 */
class DesktopNeedStockout extends BaseDesktop {
    
    public function run() {
        import("@.Workflow.Workflow");
        $workflow = new Workflow("Stockout");
        $ids = $workflow->getRowIdsByNode("StartProcess");
        
        $stockout = D("Stockout");
        $map = array(
            "id" => array("IN", $ids)
        );
        $this->data = $stockout->where($map)->limit($this->limit)->select();
    }
    
}
