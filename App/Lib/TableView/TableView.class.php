<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TableViewNew
 *
 * @author 志鹏
 */
class TableView {
    protected $hasTable = false;
    
    protected $tableStruct;
    
    public $onlyList = false;
    
    public function __construct($tableName, $groupName = GROUP_NAME) {
        if($groupName) {
            $tableName = $groupName."/".$tableName;
        }
        $file = sprintf("%s/Conf/TableView/%s.php", APP_PATH, $tableName);
        
        if(file_exists_case($file)) {
            $this->tableStruct = require($file);
            $this->hasTable = true;
        }
    }
    
    public function makeHorizontalTable($data, $pages=null, $onlyList) {
        if(!$this->hasTable) {
            return false;
        }
        
        if(!$data) {
            $view = Think::instance("View");
            $view->assign("NO_DATA", true);
        }
        
        $tableTemplate = '<table %s>%s%s%s</table>';
        $theadTemplate = '<thead><tr>%s</tr></thead>';
        $tbodyTemplate = '<tbody>%s</tbody>';
        $tfootTemplate = '<tfoot>%s</tfoot>';
        $fieldsNum = count($this->tableStruct["fields"]);
        
        //thead
        foreach($this->tableStruct["fields"] as $k=>$field) {
            if(!$k or is_int($k)) {
                $k = $field;
                $field = array();
            }
            
            $label = $field["label"] ? $field["label"] : $k;
            
            if($this->tableStruct["sortable"]) {
                if($_REQUEST["order"] == $k) {
                    $sort = $_REQUEST["sort"] ? 0 : 1;
                    $sortClass = $sort ? "icon-arrow-up" : "icon-arrow-down";
                    $icon = sprintf(' <i class="icon %s"></i> ', $sortClass);
                } else {
                    $sort = 0;
                    $icon = "";
                }
                
                $thead[] = sprintf('<th><a href="javascript:sortBy(\'%s\', %d)">%s</a>%s</th>', $k, $sort, L($label), $icon);
            } else {
                $thead[] = sprintf('<th>%s</th>', L($label));
            }
            
        }
        
        
        if(false !== $this->tableStruct["actions"]) {
            $thead[] = sprintf('<th>%s</th>', L("operation"));
        }
        
        foreach($data as $k=>$v) {
            
            $tmp = '<tr>%s</tr>';
            $tds = array();
            if($this->tableStruct["checkbox"]) {
                $fieldsNum++;
            }
            
            foreach($this->tableStruct["fields"] as $lb=>$field) {
                
                if(!$lb or is_int($lb)) {
                    $lb = $field;
                    $field = array();
                }
                if($field) {
                    $sourceFieldVal = $v[$lb];
                    $decorateParam = $v[$lb];
                    foreach($field as $k=>$extraAttr) {
                        switch($k) {
                            case "fullParams":
                                if($extraAttr) {
                                    $decorateParam = $v;
                                } else {
                                    $decorateParam = $v[$lb];
                                }
                                break;
                            case "decorate":
                                $v[$lb] = $extraAttr($decorateParam);
                                break;
                            case "link":
                                $v[$lb] = sprintf('<a href="%s">%s</a>', U(sprintf($extraAttr, $sourceFieldVal)), $v[$lb]);
                                break;
                            case "label":
                            default:
                                break;
                        }
                    }
                }
                
                //工作流中状态支持
                if($lb == "status" and $v["processes"]) {
                    //processes.currentNode.status_class
                    $sourceStatus = $v["status"];
                    $v["status"] = sprintf('<span class="badge badge-%s">%s</span>',
                            $v["processes"]["currentNode"]["status_class"],
                            $v["processes"]["currentNode"]["status_text"]
                        );
                }
                
                $tds[] = sprintf('<td>%s</td>', $v[$lb]);
            }
            $itemActions = array();
            if(false !== $this->tableStruct["actions"]) {
                foreach($this->tableStruct["actions"] as $label => $a) {
                    list($act, $class, $condition) = explode("|", $a);
                    if($condition) {
                        list($cf, $va) = explode("=", $condition);
                        if($v[$cf] != $va) {
                            continue;
                        }
                    }
                    if($class) {
                        $icon = sprintf('<i class="icon icon-%s"></i> ', $class);
                    } else {
                        $icon = L($label);
                    }

                    if(substr($act, 0, 11) == "javascript:") {
                        $itemActions[] = sprintf(sprintf('<a href="#nogo" onclick="%s" title="%s"> %s </a> ', $act,  L($label), $icon), $v["id"]);
                    } else {
                        $itemActions[] = sprintf(sprintf('<a href="%s" title="%s"> %s </a> ', U(sprintf($act, $v["id"])), L($label), $icon));
                    }
                    
                }
                //加入工作流支持 
                if($v["processes"]) {
                    if(0 == $sourceStatus and false !== $this->tableStruct["useDefaultAction"]) {
                        $itemActions[] = sprintf('<a href="%s">%s</a>',
                                "javascript:editDetail('{$v["id"]}');", '<i class="icon icon-edit"> </i>');
                        $itemActions[] = sprintf('<a href="%s">%s</a>',
                                "javascript:del('{$v["id"]}');", '<i class="icon icon-remove"> </i>');
                    } else {
                        $itemActions[] = sprintf('<a href="%s">%s</a>',
                                "javascript:viewDetail('{$v["id"]}');", '<i class="icon icon-list"> </i>');
                    }
                    foreach($v["processes"]["nextActions"] as $lpn) {
                        $itemActions[] = sprintf('<a href="%s" class="label label-%s}">%s</a>',
                                __URL__."/doWorkflow/node_id/{$lpn["id"]}/mainrow_id/{$v["id"]}", 
                                $lpn["btn_class"],
                                $lpn["name"]);
                    }
                    
                }
                $tds[] = sprintf('<td>%s</td>', implode(" ", $itemActions));
            }
            $tbody[] = sprintf($tmp, implode('', $tds));
        }
        
        /**
         * 使用默认table样式
         */
        if(false !== $this->tableStruct["useDefaultTheme"]) {
            $this->tableStruct["tableAttr"]["class"] .= " table table-bordered table-striped with-check";
        }
        
        foreach($this->tableStruct["tableAttr"] as $k=>$v) {
            $tableAttr[] = sprintf('%s="%s"', $k, $v);
        }
        
        if(false !== $this->tableStruct["actions"]) {
            $fieldsNum++;
        }
        
        if($this->tableStruct["pages"] and $pages) {
            $pages = sprintf('<div class="pagination alternate"><ul>%s</ul></div>', $pages);
            $tfoot[] = sprintf($tfootTemplate, sprintf('<tr><td colspan="%s">%s</td></tr>', 
                    $fieldsNum, $pages)
            );
        }
        
        if($this->onlyList) {
            return sprintf($theadTemplate, implode("", $thead)).
                sprintf($tbodyTemplate, implode("", $tbody));
        } else {
            $html = sprintf($tableTemplate,
                implode(" ", $tableAttr),
                sprintf($theadTemplate, implode("", $thead)),
                sprintf($tbodyTemplate, implode("", $tbody)),
                implode("", $tfoot)
            );
        }
        
//        var_dump($html);exit;?
        return $html;
    }
    
}
