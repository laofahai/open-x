<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RelationshipCompanyLinkman
 *
 * @author 志鹏
 */
class RelationshipCompanyLinkmanAction extends CommonAction {
    
    
    public function _before_insert() {
        $_POST["dateline"] = CTS;
    }
    
    public function _after_insert() {
        $this->redirect("/CRM/RelationshipCompany/viewDetail/id/".$_POST["relationship_company_id"]);
    }
    public function _after_update() {
        $this->redirect("/CRM/RelationshipCompany/viewDetail/id/".$_POST["relationship_company_id"]);
    }
    
    public function ajax_getLinkman() {
        $id = abs(intval($_GET["company_id"]));
        $customerLinkman = D("RelationshipCompanyLinkman");
        $data = $customerLinkman->where("relationship_company_id=".$id)->select();
//        print_r($data);
        $this->ajaxReturn($data);
    }
    
}
