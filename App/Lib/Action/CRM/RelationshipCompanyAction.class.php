<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RelationshipCompanyAction
 *
 * @author 志鹏
 */
class RelationshipCompanyAction extends CommonAction {
    
    public $relation = true;
    
    protected function _filter(&$map) {
        parent::_filter($map);
        $map["status|user_id"] = array('1', $this->user["id"],'_multi'=>true);
    }
    
    public function index() {
        
        $map = $this->_search();
//        $map[""]
        $where = array(
            "status" => 1,
            "saler_id" => $_SESSION["user"]["id"],
            "_logic" => "or"
        );
        $map['_complex'] = $where;
        $this->_filter($map);
        
        parent::index("RelationshipCompany", true);
    }
    
//    public function update() {
//        print_r($_POST);exit;
//    }
    
    public function insert() {
        $_POST["dateline"] = CTS;
//        $_POST["status"] = 1;
        $_POST["user_id"] = $_SESSION["user"]["id"];
        $_POST["is_customer"] = $_POST["is_customer"] ? 1 :0;
        $_POST["is_supplier"] = $_POST["is_supplier"] ? 1 :0;
        unset($_POST["is_customer"]);
        unset($_POST["is_supplier"]);
        $id = parent::insert(true);
        if($_POST["insert_linkman"] and $id) {
            $data = array(
                "relationship_company_id" => $id,
                "contact" => $_POST["contact"],
                "phone1"  => $_POST["phone"],
                "dateline"=> $_POST["dateline"],
                "email"   => $_POST["email"],
                "dateline"=> CTS,
                "region_id"=> $_POST["region_id"]
            );
            $RelationshipCompanyLinkman = D("RelationshipCompanyLinkman");
            $RelationshipCompanyLinkman->add($data);
        }
        
        if($_POST["goto"]) {
            $this->redirect($_POST["goto"].sprintf("/customer_id/%d/rel_company_name/%s", $id, $_POST["name"]));
        } else {
            $this->success(L("operate_success"));
        }
    }
    
    public function viewDetail() {
        $id = abs(intval($_GET["id"]));
        $model = D("RelationshipCompany");
        $theCustomer = $model->relation(true)->find($id);
        $this->assign("theCustomer", $theCustomer);
        $this->display();
    }
    
    
    /**
     * Ajax获取客户列表
     */
    public function ajax_getCompanys() {
        $name = trim(strip_tags($_GET["term"]));
        $customer = D("RelationshipCompany");
        $map["status|user_id"] = array('1', $this->user["id"],'_multi'=>true);
        $map["name"] = array("LIKE", "%".$name."%");
        $data = $customer->where($map)->select();
//        echo $customer->getLastSql();
        if(!$data) {
            $this->ajaxReturn(array());
        }
        
        foreach($data as $k=>$v) {
            $_data[] = array(
                "id" => $v["id"],
                "label" => $v["name"],
                "value" => $v["name"]
            );
        }
        return $this->ajaxReturn($_data);
    }
    
    public function ajax_getCompanyDataTable() {
        $customer = D("RelationshipCompany");
        switch($_GET["com_filter"]) {
            case "customer":
                $map["is_customer"] = 1;
            case "supplier":
                $map["is_supplier"] = 1;
            default:
        }
        $data = $customer->where($map)->select();
        $this->assign("data", $data);
        $this->display("../Common/Inc/RelationCompanySelectForm");
    }
    
}
