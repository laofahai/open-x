<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FinanceRecordAction
 *
 * @author 志鹏
 */
class FinanceRecordAction extends CommonAction {
    
    public function insert() {
        $model = D("FinanceRecord");
        $rs = $model->addRecord($_POST);
        if(false === $rs) {
            $this->error(L("operate_failed"));
        } else {
            $this->success(L("operate_success"));
        }
    }
    
    public function foreverdelete() {
        $id = abs(intval($_GET["id"]));
        $model = D("FinanceRecord");
        if(false === $model->delRecord($id)) {
            $this->error(L("operate_failed"));
        } else {
            $this->success(L("operate_success"));
        }
    }
    
}
