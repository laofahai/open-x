<?php

/**
 * @filename FinancePlanAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-8  8:51:58
 * @Description
 * 
 */
class FinancePlanAction extends CommonAction {
    
    public function index() {
        parent::index($this->modelName);
    }
    
    public function insert() {
        
        $id = parent::insert(true);
        
        import("@.Workflow.Workflow");
        $workflow = new Workflow($this->workflowAlias);
        $node = $workflow->doNext($id, "", true);
        
        $this->success(L("operate_success"));
    }
    
}

?>
