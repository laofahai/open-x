<?php

/**
 * @filename MessageAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-27  17:23:48
 * @Description
 * 
 */
class MessageAction extends CommonAction {
    
    public function inbox() {}
    
    public function outbox() {}
    
    public function trash() {}
    
    public function send() {}
    
}

?>
