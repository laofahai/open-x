<?php

/**
 * @filename ChatAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-27  17:19:46
 * @Description
 * 
 */
class ChatAction extends CommonAction {
    
    public function index() {
        $this->display();
    }
    
}

?>
