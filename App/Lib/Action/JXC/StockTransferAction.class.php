<?php

/**
 * @filename StockTransferAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-15  8:49:34
 * @Description
 * 
 */
class StockTransferAction extends PaperAboutAction {
    
    protected $modelName = "StockTransfer";
    
    protected $modelDetailName = "StockTransferDetail";
    
    protected $mainRowIdField = "stock_transfer_id";
    
    protected $workflowAlias = "stocktransfer";
    
    protected $ajaxRowFields = array(
        "factory_code_all"=>"","goods_name"=>"","color_name"=>"",
        "standard_name"=>"", "num"=>"input.",
        "store_num"=>"span.badge badge-info"
    );
    
    protected $fromStockField = "outstock_id";
    

    
    /**
     * 插入新单之后，开始工作流
     */
    public function insert() {
//        print_r($_SESSION);exit;
        $id = parent::insert(true);
//        var_dump($id);exit;
        if(!$id) {
            $this->errorPage("500");
        }
        import("@.Workflow.Workflow");
        $workflow = new Workflow($this->workflowAlias);
        $node = $workflow->doNext($id, "", true);
        $this->redirect("/JXC/StockTransfer/editDetail/id/".$id);
        
//        $this->success(L("operate_success"));
    }
    
}

?>
