<?php

/**
 * @filename StockinAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-13  13:19:26
 * @Description
 * 
 */
class StockinAction extends PaperAboutAction {
    
    protected $modelName = "Stockin";
    
    protected $modelDetailName = "StockinDetail";
    
    protected $mainRowIdField = "stockin_id";
    
    protected $workflowAlias = "stockin";
    
    protected $ajaxRowFields = array(
        "factory_code_all"=>"","goods_name"=>"","color_name"=>"",
        "standard_name"=>"", "num"=>"input."
    );
    
    protected $relationModel = "Returns,Purchase";
    
    
    public function _before_add() {
        $stock = D("Stock");
        $stocks = $stock->select();
        $stocks = $stock->getIndexArray($stocks);
        $this->assign("theStocks", $stocks);
    }
    
    
    /**
     * 插入新入库单之后，开始入库单工作流
     */
    public function insert() {
        $id = parent::insert(true);
        import("@.Workflow.Workflow");
        $workflow = new Workflow($this->workflowAlias);
        $node = $workflow->doNext($id, "", true);
        
        $this->redirect("/JXC/Stockin/editDetail/id/".$id);
    }
        
}

?>
