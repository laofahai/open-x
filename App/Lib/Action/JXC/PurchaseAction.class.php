<?php

/**
 * @filename PurchaseAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-7  9:32:10
 * @Description
 * 
 */
class PurchaseAction extends PaperAboutAction {
    
    protected $modelName = "Purchase";
    
    protected $modelDetailName = "PurchaseDetail";
    
    protected $mainRowIdField = "purchase_id";
    
    protected $workflowAlias = "purchase";
    
    protected $ajaxRowFields = array(
        "factory_code_all"=>"","goods_name"=>"","color_name"=>"",
        "standard_name"=>"", "per_price"=>"input","num"=>"input.",
        "store_num"=>"span.badge badge-info","price"=>"input"
    );
    
    protected $relationModel = "Stockin";
    
    /**
     * 插入新入库单之后，开始入库单工作流
     */
    public function insert() {
//        print_r($_SESSION);exit;
        $id = parent::insert(true);
//        var_dump($id);exit;
        if(!$id) {
            $this->errorPage("500");
        }
        import("@.Workflow.Workflow");
        $workflow = new Workflow($this->workflowAlias);
        $node = $workflow->doNext($id, "", true);
//        var_dump($node);exit;
        
        $this->redirect("/JXC/Purchase/editDetail/id/".$id);
        
//        $this->success(L("operate_success"));
    }
    
}

?>
