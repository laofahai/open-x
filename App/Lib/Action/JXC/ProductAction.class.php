<?php

/**
 * @filename ProductAction.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-13  16:11:20
 * @Description
 * 
 */
class ProductAction extends CommonAction {
    
    
    /**
     * @ajax
     * 根据编码搜索
     * 编码格式：产品编号-颜色编码-规格编码 按需添加相应字段
     */
    public function ajax_searchByCode() {
        $keyword = trim($_GET["keyword"]);
        if(!$keyword) {
            $this->ajaxReturn();
        }
        
        list($factoryCode,$colorid,$standardid) = explode("-", $keyword);
        $goodsModel = D("GoodsView");
        
        $map = array(
            "s.stock_id" => $_GET["stock_id"]
        );
        
        if($factoryCode) {
            $map["g.factory_code"] = $factoryCode;
        }
        if($colorid) {
            $map["g.color_id"] = $colorid;
        }
        if($standardid) {
            $map["g.standard_id"] = $standardid;
        }
        
        $data = $goodsModel
                ->table(C("DB_PREFIX")."goods_view g")
                ->field("g.*,s.num")
                ->join(C("DB_PREFIX")."stock_product_list s ON s.factory_code_all=g.factory_code_all")
                ->where($map)->limit(20)->select();
        $this->ajaxReturn($data);
    }
    
    public function ajax_searchByName() {
        $map["g.name"] = array("LIKE", "%{$_GET["keyword"]}%");
        $goodsModel = D("GoodsView");
        $data = $goodsModel
                ->table(C("DB_PREFIX")."goods_view g")
                ->field("g.*,s.num")
                ->join(C("DB_PREFIX")."stock_product_list s ON s.factory_code_all=g.factory_code_all")
                ->where($map)->limit(20)->select();
        $this->ajaxReturn($data);
    }
    
}

?>
