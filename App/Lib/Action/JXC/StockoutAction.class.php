<?php

/**
 * @filename StockoutAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-18  11:38:31
 * @Description
 * 
 */
class StockoutAction extends CommonAction {
    
    protected $modelName = "Stockout";
    
    protected $modelDetailName = "StockoutDetail";
    
    protected $mainRowIdField = "stockout_id";
    
    protected $workflowAlias = "stockout";
    
    public function index() {
        parent::index("StockoutView");
    }
    
    public function viewDetail() {
        $id = abs(intval($_GET["id"]));
        if(!$id) {
            $this->error(L("params_error"));
        }
        
        
        $viewModel = D("StockoutOrdersRelation");
        $thePaper = $viewModel->relation(true)->find($id);
//        print_r($thePaper);exit;
        
//        $viewModel->switchModel("Adv");
//        $rs = $viewModel->switchModel("View");
//        var_dump($rs);exit;
//        print_r($thePaper);exit;
//        $thePaper["souce_row"] = D($thePaper["source_model"])->find($thePaper["source_id"]);
        
        $paperDetail = D("Stockout".$thePaper["source_model"]."DetailView");
        $thePaperDetail = $paperDetail->where("StockoutDetail.stockout_id=".$thePaper["id"])->select();
        
//        print_r($thePaperDetail);exit;

        $this->assign("list", $thePaperDetail);
        $this->assign("thePaper", $thePaper);
        
        $this->display();
    }
    
}

?>
