<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StockProductList
 *
 * @author 志鹏
 */
class StockProductListAction extends CommonAction {
    
    /**
     * 仓库中产品清单
     */
    public function index() {
        $map = $this->_search('StockProductListView');
        $id = abs(intval($_GET["stock_id"]));
        if($id) {
            $map["StockProductList.stock_id"] = abs(intval($id));
        }
        
        $stock = D("Stock");
        $stocks = $stock->select();
        $this->assign("stocks", $stocks);

        
        if($id) {
            $theStock = $stock->find($id);
            $this->assign("theStock", $theStock);
            $totalNum = $theStock["total_num"];
        }
        
        $this->assign("totalNum",$totalNum);
        
        if (method_exists($this, '_filter')) {
            $this->_filter($map);
        }
        
//        $this->pageExtraUrl = "JXC/StockProductList/index//p";
        $model = D("StockProductListView");
        $this->_list($model, $map);
        $this->display();
    }
    
}
