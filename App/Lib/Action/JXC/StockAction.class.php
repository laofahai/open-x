<?php

/**
 * @filename StockAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-11  17:07:16
 * @Description
 * 
 */
class StockAction extends CommonAction {
    
    public $quickActionTpl = "baseData";
    
    public function _before_insert() {
        $_POST["managers"] = implode(",", $_POST["managers"]);
    }
    
    public function _before_update() {
        $_POST["managers"] = implode(",", $_POST["managers"]);
    }
    
    function edit() {
        $model = M("Stock");
        $id = $_REQUEST ["id"];
        $vo = $model->getById($id);
        
        if(!$vo) {
            $this->errorPage(404);
            return;
        }
        
        $vo["managers"] = explode(",", $vo["managers"]);
        
        import("@.Form.Form");
        $form = new Form(MODULE_NAME);
        $form->isEdit = true;
        $this->assign("FormHTML", $form->makeForm(null, $vo));
        
        $this->assign('vo', $vo);
        $this->display();
    }
    
    public function warning() {
        $map = $this->_search('StockProductListView');
        $id = abs(intval($_GET["stock_id"]));
        $stock = D("Stock");
        $stocks = $stock->select();
        $this->assign("stocks", $stocks);
        
        if(!$id and $stocks) {
            $this->redirect("/JXC/Stock/warning/stock_id/".$stocks[0]["id"]);
        }
        
        $this->assign("theStock", $stock->find($id));
        
        $goodsView = D("GoodsView");
        
        if (isset($_REQUEST ['order'])) {
            $order = $_REQUEST ['order'];
        } else {
            $order = "store_num";
        }
        
        if (isset($_REQUEST ['sort'])) {
            $sort = $_REQUEST ['sort'] ? 'asc' : 'desc';
        } else {
            $sort = $asc ? 'asc' : 'desc';
        }
        
        $data = $goodsView->table(C("DB_PREFIX")."goods_view GoodsView")
                  ->field("GoodsView.*,spl.num as store_num")
                  ->join(C("DB_PREFIX")."stock_product_list spl ON spl.goods_id=GoodsView.id AND spl.color_id=GoodsView.color_id AND spl.standard_id=GoodsView.standard_id")
                  ->where("GoodsView.store_min>spl.num AND spl.stock_id=".$id)
                  ->order("`" . $order . "` " . $sort)
                  ->select();
        
        $sortImg = $sort == "desc" ? "icon-arrow-up" : "icon-arrow-down"; //排序图标
        $sort = $sort == 'desc' ? 1 : 0; //排序方式
        
        $this->assign('sortField', $_GET["order"]);
        $this->assign('sortImg', $sortImg);
        $this->assign("sort", $sort);
        $this->assign("list", $data);
        $this->display();
    }
    
    public function ajax_getWarningNum() {
        $stock = D("Stock");
        $stocks = $stock->select();
        $stockid = abs(intval($_GET["stock_id"]));
        if(!$stockid and $stocks) {
            $stockid = $stocks[0]["id"];
        }
        $goodsView = D("GoodsView");
        $data = $goodsView->table(C("DB_PREFIX")."goods_view GoodsView")
                  ->field("GoodsView.*,spl.num as store_num")
                  ->join(C("DB_PREFIX")."stock_product_list spl ON spl.goods_id=GoodsView.id AND spl.color_id=GoodsView.color_id AND spl.standard_id=GoodsView.standard_id")
                  ->where("GoodsView.store_min>spl.num AND spl.stock_id=".$stockid)
                  ->count();
        $this->ajaxReturn(array("num"=>$data));
    }
    
}

?>
