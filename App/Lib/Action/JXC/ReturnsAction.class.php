<?php

/**
 * @filename ReturnsAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-30  10:59:08
 * @Description
 * 
 */
class ReturnsAction extends PaperAboutAction {
    
    protected $modelName = "Returns";
    
    protected $modelDetailName = "ReturnsDetail";
    
    protected $mainRowIdField = "returns_id";
    
    protected $workflowAlias = "returns";
    
    protected $ajaxRowFields = array(
        "factory_code_all"=>"","goods_name"=>"","color_name"=>"",
        "standard_name"=>"", "num"=>"input.",
        "store_num"=>"span.badge badge-info"
    );
    
    protected $relationModel = "Stockin";
    

    
    /**
     * 插入新入库单之后，开始入库单工作流
     */
    public function insert() {
//        print_r($_SESSION);exit;
        $id = parent::insert(true);
//        var_dump($id);exit;
        if(!$id) {
            $this->errorPage("500");
        }
        import("@.Workflow.Workflow");
        $workflow = new Workflow($this->workflowAlias);
        $node = $workflow->doNext($id, "", true);
        $this->redirect("/JXC/Returns/editDetail/id/".$id);
        
//        $this->success(L("operate_success"));
    }
            
}

?>