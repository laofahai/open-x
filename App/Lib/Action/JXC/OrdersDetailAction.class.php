<?php

/**
 * @filename OrdersDetailAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-21  15:01:09
 * @Description
 * 
 */
class OrdersDetailAction extends CommonAction {
    
    public function index() {
        parent::index("OrdersDetailView");
    }
    
}

?>
