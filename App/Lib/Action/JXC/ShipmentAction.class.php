<?php

/**
 * @filename ShipmentAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-6  11:24:28
 * @Description
 * 
 */
class ShipmentAction extends CommonAction {
    
    public function viewDetail() {
        $id = abs(intval($_GET["id"]));
        $model = D("ShipmentView");
        $vo = $model->find($id);
        $this->assign("vo",$vo);
        $this->display();
    }
    
}

?>
