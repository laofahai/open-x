<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OrdersAction
 *
 * @author 志鹏
 */
class OrdersAction extends PaperAboutAction {
    
    protected $modelName = "Orders";
    
    protected $modelDetailName = "OrdersDetail";
    
    protected $mainRowIdField = "order_id";
    
    protected $workflowAlias = "order";
    
    protected $ajaxRowFields = array(
        "factory_code_all"=>"","goods_name"=>"","color_name"=>"",
        "standard_name"=>"", "per_price"=>"input","num"=>"input.",
        "store_num"=>"span.badge badge-info","price"=>"input"
    );
    
    protected $relationModel = "Stockout";
    
    public function _before_add() {
        $stock = D("Stock");
        $stocks = $stock->select();
        $stocks = $stock->getIndexArray($stocks);
        $this->assign("theStocks", $stocks);
    }
    
    
    /**
     * 插入新入库单之后，开始入库单工作流
     */
    public function insert() {
//        echo "<pre>";
//        print_r($_POST);
        $id = parent::insert(true);
        if(!$id) {
            $this->errorPage("500");
        }
        import("@.Workflow.Workflow");
        $workflow = new Workflow($this->workflowAlias);
        $node = $workflow->doNext($id, "", true);
        $this->redirect("/JXC/Orders/editDetail/id/".$id);
        
//        $this->success(L("operate_success"));
    }
            
}

?>
