<?php

/**
 * @filename CommonStatisticsAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-28  15:21:48
 * @Description
 * 
 */
class CommonStatisticsAction extends CommonAction {
    
    /*
     * 销售，走势图
     * **/
    protected function ForSaleTotal($data, $start, $end, $step, $format="m-d") {
        
        $dateRange = makeDateRange($start, $end, $step, $format);
        
        foreach($dateRange as $i=>$dr) {
            $tmp[$dr]["total"] = 0;
            $tmp[$dr]["y"] = 0;
            $tmp[$dr]["color"] = rand(0,10);
            foreach($data as $k=>$v) {
                $key = date($format, $v["dateline"]);
                if($dr == $key) {
                    $tmp[$dr]["y"]+=$v["total_price_real"];
                    $tmp[$dr]["total"]+=1;
                }
            }
        }
//        print_r($data);exit;
        $data = ksortHaveNoIndex($tmp);
        $this->assign("categories", json_encode($dateRange));
        $this->assign("data", str_replace('"', '', json_encode($data)));
        
    }
    
    protected function ForCustomerRange($data) {
        $this->getRange($data, "customer_id", "rel_company_name");
    }
    
    protected function ForSalerRange($data) {
        $this->getRange($data, "saler_id", "sponsor");
    }
    
    private function getRange($data, $field, $nameField) {
        foreach($data as $k=>$v) {
            $tmp[$v[$field]]["color"] = rand(0,10);
            $tmp[$v[$field]]["y"] += $v["total_price_real"];
            $tmp[$v[$field]]["total"] += 1;
            $items[$v[$field]] = $v[$nameField];
        }
        
        $tmp = array_sort($tmp, "y", "desc");
        
        foreach ($tmp as $k=>$v) {
            $cats[] = $items[$k];
        }
        
        $tmp = ksortHaveNoIndex($tmp);
        for($i=0;$i<15;$i++) {
            if(!$tmp[$i]) {
                continue;
            }
            $theData[] = $tmp[$i];
            $theCats[] = $cats[$i];
        }
        $this->assign("categories", json_encode($theCats));
        $this->assign("data", str_replace('"', '', json_encode($theData)));
    }
    
    protected function ForStockin() {}
    
    protected function StockinLine() {}
    
}

?>
