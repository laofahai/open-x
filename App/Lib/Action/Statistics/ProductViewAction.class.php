<?php

/**
 * @filename ProductViewAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-16  16:02:30
 * @Description
 * 
 */
class ProductViewAction extends CommonAction {
    
    public function index() {
        
        if($_GET["stock_id"]) {
            $map["stock_id"] = $_GET["stock_id"];
        }
        
        $this->makeFilter();
        $type = $_GET["type"] ? $_GET["type"] : "product";
        switch($type) {
            case "category":
                $voList = $this->getByCategory($map);
                break;
            case "product":
                $voList = $this->getByProduct($map);
        }
        
        import("@.TableView.TableView");
        $table = new TableView(MODULE_NAME);
        $this->assign("TableView", $table->makeHorizontalTable($voList, $this->page));
        $this->display();
    }
    
    private function getByCategory($map) {}
    
    /**
     * 
     */
    private function getByProduct($map) {
        $storeProductModel = D("StockProductList");
        $count = $storeProductModel->where($map)->count();
//        echo $model->getLastSql();exit;
        if ($count <= 0) {
            return;
        }
        
//        import("ORG.Util.Page");
//        //创建分页对象
//        $p = new Page($count, 20);
//        //分页查询数据
//        if($this->pageExtraUrl) {
//            $p->url = $this->pageExtraUrl;
//        }
//        ->limit($p->firstRow . ',' . $p->listRows)

        $storeProductViewModel = D("StockProductListView");
        $theStoreProducts = $storeProductViewModel->where($map)->select();
        
//        $this->page = $p->show();
        
        foreach($theStoreProducts as $v) {
            $factoryCodes[] = $v["factory_code_all"];
        }
        
        $map["factory_code_all"] = array("IN", implode(",", $factoryCodes));
        
        $stock_id = $map["stock_id"];
        unset($map["stock_id"]);
        
        $ordersDetailModel = D("OrdersDetail");
        $theOrdersDetail = $ordersDetailModel->relation(true)->where($map)->select();
//        echo $ordersDetailModel->getLastSql();exit;
        foreach($theOrdersDetail as $k=>$v) {
            if($v["Orders"]["status"] < 1) { // 状态小于1 无效订单
                continue;
            }
            if($stock_id and $v["Orders"]["stock_id"] !== $stock_id) {
                continue;
            }
            $rsOrders[$v["factory_code_all"]]["amount"]+= $v["num"];
            $rsOrders[$v["factory_code_all"]]["price"]+= $v["price"];
        }
        
        if(isModuleEnabled("Purchase")) {
            $purchaseDetailModel = D("PurchaseDetail");
            $thePurchaseDetail = $purchaseDetailModel->relation(true)->where($map)->select();
//            print_r($thePurchaseDetail);exit;
            foreach($thePurchaseDetail as $k=>$v) {
                if($v["Purchase"]["status"] < 1) { // 状态为1 无效订单
                    continue;
                }
                if($stock_id and $v["Purchase"]["stock_id"] !== $stock_id) {
                    continue;
                }
                $rsPurchase[$v["factory_code_all"]]["amount"]+= $v["num"];
                $rsPurchase[$v["factory_code_all"]]["price"]+= $v["price"];
            }
        }
        
        
        foreach($theStoreProducts as $tsp) {
            $fca = $tsp["factory_code_all"];
            
            if($rsOrders[$fca]["amount"] <=0) {
                continue;
            }
            
            $volist[$fca]["factory_code_all"] = $fca;
            $volist[$fca]["goods_name"] = $tsp["goods_name"];
            $volist[$fca]["standard_name"] = $tsp["standard_name"];
            $volist[$fca]["color_name"] = $tsp["color_name"];
            $volist[$fca]["purchase_amount"] = $rsPurchase[$fca]["amount"];
            $volist[$fca]["purchase_price"] = sprintf("%.2f", $rsPurchase[$fca]["price"]);
            $volist[$fca]["sale_amount"] = $rsOrders[$fca]["amount"];
            $volist[$fca]["sale_price"] = sprintf("%.2f", $rsOrders[$fca]["price"]);
            $volist[$fca]["store_num"] = $tsp["num"];
            
//            $orderBy[] = $volist[$fca][$_GET["order"]];
        }
        $sort = $_GET["sort"] == 1 ? "desc" : "asc";
        $volist = array_sort($volist, $_GET["order"], $sort);
//        array_multisort($orderBy, $sort, $data);
//        var_dump($rsOrders);exit;
        return $volist;
    }
    
}

?>
