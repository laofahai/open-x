<?php

/**
 * @filename StockinAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-21  15:16:08
 * @Description
 * 
 */
class StockinAction extends CommonAction {
    
    public function index() {}
    
    public function ajax_StockinDaysLine() {
        $days = abs(intval($_GET["days"]));
        $days = $days ? $days : 10;
        $endTime = CTS;
        $startTime = CTS-3600*24*$days;
        
        $stockin = D("Stockin");
        $map = array(
            "dateline" => array("BETWEEN", array($startTime, $endTime)),
            "status" => array("GT", "1")
        );
        $papers = $stockin->where($map)->select();
//        echo $stockin->getLastSql();exit;
//        print_r($papers);exit;
        $tmp = range($startTime, $endTime, 24*3600);
        $dateRange = array_map(create_function('$v', 'return date("m-d", $v);'), $tmp);
        
        foreach($dateRange as $d) {
            $data[$d] = 0;
            foreach($papers as $o) {
                $key = date("m-d", $o["dateline"]);
                if($key == $d) {
                    $data[$d] += $o["total_num"];
                }
            }
        }
//        print_r($data);
//        exit;;
//        print_r()
        $data =ksortHaveNoIndex($data);
        
        
        $this->ajaxReturn(array(
            "dateRange" => $dateRange,
            "series" => array(
                array(
                "name"=>"入库数",
                "data" => $data
            ))
        ));
    }
    
}

?>
