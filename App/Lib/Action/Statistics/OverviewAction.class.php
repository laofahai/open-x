<?php

/**
 * @filename OverviewAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-12  10:51:01
 * @Description
 * 
 */
class OverviewAction extends CommonStatisticsAction {
    
    /**
     * 总览，显示start-end中，
     * 模块：销售 数量、销售额
     * 财务：收入、开始、利润
     * 库存：入库数量、出库数量、退货数量
     * 客户：总量、新增量
     * 
     */
    public function index() {
        
        $starttime = $_GET["date_start"] ? strtotime($_GET["date_start"]) : strtotime(date("Y-01-01", CTS));
        $endtime = $_GET["date_end"] ? strtotime($_GET["date_end"]) : CTS;
        
        $map = array(
            "dateline" => array("BETWEEN", array($starttime, $endtime)),
            "status"   => array("GT", 0)
        );
        
        $orderModel = D("Orders");
        $order = $orderModel->field("SUM(total_price) AS amount, SUM(total_num) AS totalnum")->where($map)->find();
        $data["sale_num"] = $order["totalnum"];
        $data["sale_amount"] = $order["amount"];
        
        $stockinModel = D("Stockin");
        $tmp = $stockinModel->field("SUM(total_num) AS totalnum")->where($map)->find();
        $data["stockin_num"] = $tmp["totalnum"];
        
        $stockoutModel = D("Stockout");
        $tmp = $stockoutModel->field("SUM(total_num) AS totalnum")->where($map)->find();
        $data["stockout_num"] = $tmp["totalnum"];
        
        $financeModel = D("FinanceRecord");
        $tmp = $financeModel->where($map)->select();
        foreach($tmp as $t) {
            $finance[$t["type"]]["amount"] += $t["amount"];
            $finance[$t["type"]]["detail"][$t["type_id"]]["amount"] += $t["amount"];
            $finance[$t["type"]]["detail"][$t["type_id"]]["type"] += $t["type_id"];
        }
        
        $data["finance_pay"] = $finance[2];
        $data["finance_receive"] = $finance[1];
        $data["finance_profit"] = $finance[1]["amount"]-$finance[2]["amount"];
        
//        print_r($data);exit;
        $this->assign("data", $data);
        $chartTitle = sprintf(L("overview")." - %s~%s", date("Y-m-d", $starttime), date("Y-m-d", $endtime));
        $this->assign("chartTitle", $chartTitle);
        $this->makeFilter();
        $this->display();
    }
    
}

?>
