<?php

/**
 * @filename IndexAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-19  9:45:53
 * @Description
 * 
 */
class IndexAction extends CommonAction {
    
    //订单图标
    /**
     * 本月，上月同期，去年同期
     */
    public function ajax_orderChart() {
        $order = D("OrdersView");
        //$map
        $data = $order->where()->select();
        
        $thisMonth = strtotime(date("Y-m", CTS));
        
        foreach($data as $d) {
            $theDate = date("d", $d["dateline"]);
            $dateData[$theDate][] = $d;
        }
        
        $thisMonthData = array(
            "label" => date("Y年m月份", CTS),
            "data" => array()
        );
        
        for($i=1;$i<=31;$i++) {
            $thisMonthData["data"][] = array(
                $i,count($dateData[$i])+rand(20,500)
            );
        }
        $thisMonthKey = date("Y年m月份",CTS);
        $return = array(
            $key => $thisMonthData,
            "sdf" => $thisMonthData
        );
        
        $this->ajaxReturn($return);
    }
    
}

?>
