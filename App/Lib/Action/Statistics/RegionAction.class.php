<?php

/**
 * @filename RegionAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-12  11:00:01
 * @Description
 * 
 */
class RegionAction extends CommonStatisticsAction {
    
    public function index() {
        
        $time = $_GET["time"];
        switch($time) {
            case "mulyear":
                $starttime = strtotime((date("Y", CTS)-3)."-00-00");
                $endtime = strtotime((date("Y", CTS)+1)."-01-02");
                $step = 24*3600*365;
                $format = "Y";
                $title = L("mulyearly_sale_bar");
                break;
            case "year":
                $starttime = strtotime(date("Y-01-01", CTS));
                $endtime = strtotime(date("Y-12-31", CTS));
                $step = 24*3600*31;
                $format = "Y-m";
                $title = L("monthly_sale_bar");
                break;
            default:
                $starttime = strtotime(date("Y-m", CTS));
                $endtime = strtotime(date("Y-m-t"));
                $step = 24*3600;
                $format = "m-d";
                $title = L("dayly_sale_bar");
                break;
        }
        
        $map = array(
            "status" => array("GT", 1),
            "dateline" => array("BETWEEN", array($starttime, $endtime))
        );
        
        $orderModel = D($model);
        $orderSourceData = $orderModel->where($map)->select();
        
        $this->ForSaleTotal($orderSourceData, $starttime, $endtime, $step, $format);
        $this->assign("chartTitle", $title);
        $this->display();
    }
    
}

?>
