<?php

/**
 * @filename AccountingVoucherAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-31  8:39:42
 * @Description
 * 
 */
class AccountingVoucherAction extends CommonAction {

    public function _before_add() {
        $period = D("AccountingPeriod")->order("id DESC")->find();
        $mark_num = D("AccountingVoucher")->where("period_id=".$period["id"])->max("mark_num");
        $this->assign("mark_num", abs(intval($mark_num)) + 1);
        $this->assign("vocher_mark", getTypes("voucher"));
        $this->assign("period", $period);
    }

    public function index() {
        $map = array();
        $model = D("AccountingVoucher");
        $relModel = $model->switchModel("Relation", array("_link", $model->_link));
        $count = $model->where($map)->count("id");
        import("ORG.Util.Page");
        //创建分页对象
        if (!empty($_REQUEST ['listRows'])) {
            $listRows = $_REQUEST ['listRows'];
        } else {
            $listRows = 15;
        }
        $p = new Page($count, $listRows);

        //分页查询数据

        if ($this->pageExtraUrl) {
            $p->url = $this->pageExtraUrl;
        }
        
        $data = $relModel->relation(true)->where($map)->limit($p->firstRow . ',' . $p->listRows)->select();

//        $data = $relModel->relation(true)->select();
//        echo $relModel->getLastSql();exit;
//        print_r($relModel);
//        print_r($data);

        $this->assign("volist", $data);
        $this->assign("pages", $p->show());
        $this->display();

//        $detail = D("AccountingVoucherDetail");
//        print_r($detail->select());
    }

    public function insert() {
        
//        echo $_POST["vchData"];
//        $_POST["vchData"] = '{"id":-1,"groupId":"17","number":"1","attachments":0,"date":"2014-01-03","year":"2013","period":"12","yearPeriod":201312,"entries":"[{\"voucherId\":-1,\"id\":-1,\"explanation\":\"差旅费\",\"accountId\":\"10\",\"accountNumber\":\"1001\",\"accountName\":\"库存现金\",\"dc\":-1,\"amount\":\"300.00\",\"itemClassName\":\"\",\"itemId\":0,\"itemNumber\":\"\",\"itemName\":\"\",\"customId\":0,\"customNumber\":\"\",\"customName\":\"\",\"deptId\":0,\"deptNumber\":\"\",\"deptName\":\"\",\"supplierId\":0,\"supplierNumber\":\"\",\"supplierName\":\"\",\"empId\":0,\"empNumber\":\"\",\"empName\":\"\",\"inventoryId\":0,\"projectId\":0,\"projectNumber\":\"\",\"projectName\":\"\",\"qty\":0,\"unit\":\"\",\"price\":0,\"cur\":\"RMB\",\"rate\":\"1\",\"amountFor\":\"RMB\",\"settleCode\":0,\"settleno\":\"\",\"settleDate\":\"\",\"transbal\":0,\"transbalFor\":0,\"transDate\":\"\",\"transno\":\"\",\"match\":0,\"qtyAux\":false,\"trans\":false,\"entryId\":1,\"transId\":-1,\"control\":false,\"accountId2\":-1,\"acctCur\":\"RMB\"},{\"voucherId\":-1,\"id\":-1,\"explanation\":\"差旅费\",\"accountId\":\"11\",\"accountNumber\":\"1002\",\"accountName\":\"银行存款\",\"dc\":1,\"amount\":\"300.00\",\"itemClassName\":\"\",\"itemId\":0,\"itemNumber\":\"\",\"itemName\":\"\",\"customId\":0,\"customNumber\":\"\",\"customName\":\"\",\"deptId\":0,\"deptNumber\":\"\",\"deptName\":\"\",\"supplierId\":0,\"supplierNumber\":\"\",\"supplierName\":\"\",\"empId\":0,\"empNumber\":\"\",\"empName\":\"\",\"inventoryId\":0,\"projectId\":0,\"projectNumber\":\"\",\"projectName\":\"\",\"qty\":0,\"unit\":\"\",\"price\":0,\"cur\":\"RMB\",\"rate\":\"1\",\"amountFor\":\"RMB\",\"settleCode\":0,\"settleno\":\"\",\"settleDate\":\"\",\"transbal\":0,\"transbalFor\":0,\"transDate\":\"\",\"transno\":\"\",\"match\":0,\"qtyAux\":false,\"trans\":false,\"entryId\":2,\"transId\":-1,\"control\":false,\"accountId2\":-1,\"acctCur\":\"RMB\"}]","debitTotal":"300.00","creditTotal":"300.00","explanation":"差旅费","userName":"","checked":0,"checkName":"","posted":0,"modifyTime":"","ownerId":1,"checkerId":1}';
        $sourceData = json_decode($_POST['vchData']);
        $sourceData->entries = json_decode($sourceData->entries);

//        print_r($sourceData);exit;

        if ($sourceData->debitTotal !== $sourceData->creditTotal) {
            return false;
        }

        $periodModel = D("AccountingPeriod");
        $period = $periodModel->where(array(
                    "year" => $sourceData->year,
                    "period" => $sourceData->period
                ))->find();

        if (!$period) {
            return false;
        }

        $data = array(
            "period_id" => $period["id"],
            "user_id" => getCurrentUid(),
            "attach_num" => $sourceData->attachments,
            "mark_id" => $sourceData->groupId,
            "mark_num" => $sourceData->number,
            "date" => strtotime($sourceData->date),
            "status" => 0,
            "total_amount" => $sourceData->debitTotal,
            "dateline" => CTS
        );

        $voucherModel = D("AccountingVoucher");
        $voucherModel->startTrans();

        $voucherId = $voucherModel->add($data);
//        echo $voucherModel->getLastSql();
        if (!$voucherId) {
            $voucherModel->rollback();
        }

        $entryModel = D("AccountingVoucherDetail");
        $success = true;
        foreach ($sourceData->entries as $entry) {
            $data = array(
                "voucher_id" => $voucherId,
                "subject_id" => $entry->accountId,
                "summary" => $entry->explanation,
                "dc" => $entry->dc > 0 ? 1 : 0,
                "amount" => $entry->amount
            );
            if (!$entryModel->add($data)) {
                $success = false;
                $voucherModel->rollback();
                break;
            }
        }
        if($success) {
            $voucherModel->commit();
            $this->ajaxReturn(array(
                "status" => 200,
                "msg" => L("operate_success")
            ));
        } else {
            $this->ajaxReturn(array(
                "status" => 500,
                "msg" => L("operate_failed")
            ));
        }
    }

}

?>
