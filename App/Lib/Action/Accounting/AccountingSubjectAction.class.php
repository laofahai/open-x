<?php

/**
 * @filename AccountingSubjectAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-30  13:56:33
 * @Description
 * 
 */
class AccountingSubjectAction extends NetestCategoryAction {
    
    public function _before_update() {
        $_POST["assist_items"] = $_POST["is_assist"] ? implode(",",$_POST["assist_items"]) : "";
    }
    
    public function _before_insert() {
        $_POST["assist_items"] = $_POST["is_assist"] ? implode(",",$_POST["assist_items"]) : "";
    }

    public function ajax_getSubject() {
        $model = D("AccountingSubject");
        $tmp = $model->getTree(1);
        foreach($tmp as $k=>$t) {
            
            $allcats[$t["id"]] = $t["name"];
            if($t["code"] < 10) {
                continue;
            }
            $t["deep"] -= 2;
            if(strlen($t["code"]) > 4) { //子分类
                $t["name"] = $allcats[$t["id"]] = $allcats[$t["pid"]]."_".$t["name"];
            }
            
            $tree[$k]["isItem"] = 0;
            if($t["is_assist"] and $t["assist_items"]) {
                $tree[$k]["isItem"] = 1;
                $itemTmp = explode(",", $t["assist_items"]);
                foreach($itemTmp as $i) {
                    $tree[$k][$i] = 1;
                }
            }
            $tree[$k]["id"] = $t["id"];
            $tree[$k]["fullName"] = $t["name"];
            $tree[$k]["number"] = $t["code"];
            $tree[$k]["value"] = $t["id"];
        }
        
        foreach($tree as $k=>$v) {
            $data["items"][] = $v;
        }
        
//        print_r($data);
        
        $this->ajaxReturn($data);
    }
    
}

?>
