<?php

/**
 * @filename AccountingAssistItemAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2014-1-3  15:12:55
 * @Description
 * 
 */
class AccountingAssistItemAction extends CommonAction {
    
    public function ajax_getItem () {
        //{"status":200,"data":{"total":1,"items":[{"id":729663775633124,"name":"你好","number":"123","type":-10}],"page":1,"records":1},"msg":"操作成功"}
        $data = array(
            "status" => 200,
            "data" => array(
                "total" => 1,
                "items" => array(
                    array(
                        "id" => 1,
                        "name"=> "客户1",
                        "number" => 1,
                    ),
                    array(
                        "id" => 2,
                        "name"=> "客户1",
                        "number" => 2,
                    ),
                    array(
                        "id" => 3,
                        "name"=> "客户1",
                        "number" => 3,
                    ),
                    array(
                        "id" => 4,
                        "name"=> "客户1",
                        "number" => 5,
                    ),
                ),
            ),
            "message" => "还行"
        );
        $this->ajaxReturn($data);
    }
    
}

?>
