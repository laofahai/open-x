<?php

/**
 * @filename RegionAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-7  16:04:50
 * @Description
 * 
 */
class RegionAction extends CommonAction {
    
    public function ajax_getRegion(){
		$Region = D("Region");
		$map['pid'] = $_REQUEST["pid"];
		$map['type']=$_REQUEST["type"];
		$list=$Region->where($map)->select();
		echo json_encode($list);
	}
    
}

?>
