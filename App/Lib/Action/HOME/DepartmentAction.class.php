<?php

/**
 * @filename DepartmentAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-18  13:27:06
 * @Description
 * 
 */
class DepartmentAction extends NetestCategoryAction {
    
    public function _after_insert() {
        $this->redirect("/HOME/Department");
    }
    
}

?>
