<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AuthGroupAction
 *
 * @author 志鹏
 */
class AuthGroupAction extends CommonAction {
    
    public function viewDetail() {
        
        $id = abs(intval($_GET["id"]));
        $groupRuleModel = D("AuthGroupRule");
        
        if(IS_POST) {
//            print_r($_POST);exit;
            $flag = $_POST["flag"] ? 1 : 0;
            $groupRuleModel->where("group_id=".$id)->delete();
            foreach($_POST["rule_id"] as $rid) {
                $data = array(
                    "group_id" => $id,
                    "rule_id"  => $rid,
                    "flag"     => $flag
                );
                $groupRuleModel->add($data);
            }
            $this->redirect("/HOME/AuthGroup/viewDetail/id/".$id);
        }
        
        
        $groupModel = D("AuthGroup");
        $theGroup = $groupModel->find($id);
        
        $tmp = $groupRuleModel->where("group_id=".$id)->select();
        
        foreach($tmp as $v){
            $selectedRules[] = $v["rule_id"];
            $flag = $v["flag"];
        }
//        $selectedRules = explode(",", $theGroup["rules"]);
        
        $ruleModel = D("AuthRule");
        $theRules = $ruleModel->where("status=1")->select();
        
        foreach($theRules as $tr){
            $rsRules[$tr["category"]][] = $tr;
        }
//        krsort();
        ksort($rsRules);
        
//        $theRules = $ruleModel->getIndexArray($theRules, "title");
        
        $this->assign("flag", $flag);
        $this->assign("theRules", $rsRules);
        $this->assign("selectedRules", $selectedRules);
        $this->display();
        
    }
    
}

?>
