<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PassportAction
 *
 * @author 志鹏
 */
class PassportAction extends CommonAction {
    
    public function index() {
        $this->redirect("/HOME/Index/dashboard");
    }
    
    public function Profile() {
        if(IS_POST) {
            if(!$_POST["password"]) {
                unset($_POST["password"]);
            } else {
                $_POST["password"] = getPwd($_POST["password"]);
            }
            $model = D("UserRelation");
            $model->where("id=".$this->user["id"])->save($_POST);
            
            $theUser = $model->relation(true)->find($this->user["id"]);
            $this->user = $_SESSION["user"] = $theUser;
//            print_r($_SESSION["user"]);exit;
//            echo $model->getLastSql();exit;
            if($_POST["password"]) {
                $this->logout();
            } else {
                $this->redirect("/HOME/Passport/Profile");
            }
            
            return;
        }
//        print_r($_SESSION["user"]);exit;
        $data = $this->user;
        unset($data["password"]);
        import("@.Form.Form");
        $form = new Form("Passport", "Profile");
        $this->assign("FormHTML", $form->makeForm(null, $data));
        $this->display();
    }
    
    public function login() {
        if(IS_POST) {
            $user = D("UserRelation");
            $theUser = $user->relation(true)->getByUsername($_POST["login"]);
//            echo $user->getLastSql();
//            var_dump($theUser);exit;
            if($theUser["status"] < 1) {
                return $this->errorPage("403");
                //@todo 禁用用户
            }
            
            if(!$theUser or $theUser["password"] !== getPwd($_POST["password"])) {
                $this->error(L("operate_failed"));
            }
            foreach($theUser["groups"] as $g) {
                $theUser["group_ids"][] = $g["id"];
            }
            
            $_SESSION["user"] = $theUser;
            $this->redirect("/HOME/Index/index");
        } else {
            $this->display();
        }
    }
    
    public function logout() {
        session_destroy();
        $this->redirect("/HOME/Passport/login");
    }
    
}

?>
