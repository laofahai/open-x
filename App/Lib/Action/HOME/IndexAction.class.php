<?php

/**
 * @filename IndexAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-11  15:33:54
 * @Description
 * 
 */
class IndexAction extends CommonAction {
    
    public function index() {
        $this->makeNav();
        $this->display();
    }
    
    /**
     * 根据AuthRule生成左侧导航，不同用户生成不同缓存
     * @todo 三级分类（快捷导航）
     */
    private function makeNav() {
        $navs = F("Nav/".$this->user["id"]);
        if($navs) {
            $this->assign("LeftNavs", $navs);
            return;
        }
        
        $navs = require APP_PATH.DS."Conf".DS."navs.php";
        
        $auth=new Auth();
        foreach($navs as $label => $n) {
            $childs = array();
            foreach($n["childs"] as $cl => $c) {
                list($group, $module, $action) = explode("/", $c);
                $action = $action ? $action : "index";
                $authName = sprintf("%s.%s.%s", $group, $module, $action);
                
                if(!$auth->check($authName, $_SESSION["user"]["id"])) {
                    continue;
                }
//                if(substr($c, 0, 1) == "#" or substr($c, 0, 11) == "javascript:") {
//                    $url = $c;
//                } else {
//                    $url = U("/".$c);
//                }
                $url = $c;
                $childs[] = array(
                    "label" => $cl,
                    "url"   => $url
                );
            }
            
            $url = $n["action"];
            if($childs or isset($n["action"])) {
                $theNav[] = array(
                    "childs" => $childs,
                    "label"  => $label,
                    "icon"   => $n["icon"],
                    "url"    => $url
                );
            }
            
        }
        F("Nav/".$this->user["id"], $theNav);
//        print_r($theNav);exit;
        $this->assign("LeftNavs", $theNav);
    }
    
    public function dashboard() {
        import("@.Desktop.Desktop");
        $desktop = new Desktop();
        $blocks = $desktop->getItems();
        $this->assign("DesktopBlocks", $blocks);
        $this->display();
    }
    
    public function ajax_Upload() {
        import('ORG.Net.UploadFile');
        $upload = new UploadFile();// 实例化上传类
        $upload->maxSize  = 3145728 ;// 设置附件上传大小
        $upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->savePath =  './Public/Uploads/uid_'.getCurrentUid()."/";// 设置附件上传目录
        $upload->autoSub = true;
        $upload->subType = "date";
        $upload->dateFormat = "Ym";
        if(!$upload->upload()) {// 上传错误提示错误信息
            $message = $upload->getErrorMsg();
            $status = 1;
        }else{// 上传成功 获取上传文件信息
            $status = 0;
            $message = L("upload_success");
            $info =  $upload->getUploadFileInfo();
            $message = __APP__.str_replace("./","/",$info[0]["savepath"]).$info[0]["savename"];
//            print_r($info);exit;
        }
        $return = array(
            'error'  => $status,
            'msg' => $message,
        );
        echo json_encode($return);
    }
    
}

?>
