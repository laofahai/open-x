<?php

/**
 * @filename SettingsAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-19  9:03:49
 * @Description
 * 
 */
class SettingsAction extends CommonAction {
    
    public function clearCache() {
        clearCache(0);
        clearCache(1);
        clearCache(2);
        clearCache(3);
        $this->success(L("operate_success"), U("/HOME/Index/dashboard"));
    }
    
}

?>
