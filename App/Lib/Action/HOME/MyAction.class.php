<?php

/**
 * @filename MyAction.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-23  11:29:15
 * @Description
 * 
 */
class MyAction extends CommonAction {
    
    /**
     * 桌面模块设置
     */
    public function Desktop() {
        
        $enabledDesktop = C("ENABLED_DESKTOP");
        
        $model = D("UserDesktop");
        if(IS_POST) {
            if($_POST["files"]) {
                $model->where("uid=".  getCurrentUid())->delete();
                foreach($_POST["files"] as $k=> $f) {
                    $data = array(
                        "uid" => getCurrentUid(),
                        "block_name" => $f,
                        "listrows" => $_POST["num"][$k],
                        "listorder"=> $_POST["listorder"][$k],
                        "position" => $_POST["position"][$k] > 1 ? 2 : 1
                    );
                    $model->add($data);
                }
                $this->redirect("/HOME/My/Desktop");
            }
        }
        
        $blocks = $model->where("uid=".getCurrentUid())->order("listorder DESC")->select();
        foreach($blocks as $b) {
            $selected_block[$b["block_name"]] = $b;
        }
        
        import('ORG.Util.Auth');//加载类库
        $auth=new Auth();
//        if(!$auth->check($rule, $_SESSION["user"]["id"])){
        
        foreach($enabledDesktop as $k=>$v) {
            list($group, $module, $action) = explode("/", $v["need"]);
            $action = $action ? $action : "index";
            $name = sprintf("%s.%s.%s", $group, $module, $action);
            if(!$auth->check($name, getCurrentUid())) {
//                echo $name;
                continue;
            }
            $enabledDesktop[$k]["file"] = $k;
            if(array_key_exists($k, $selected_block)) {
                $enabledDesktop[$k]["selected"] = true;
                $enabledDesktop[$k]["num"] = $selected_block[$k]["listrows"];
                $enabledDesktop[$k]["listorder"] = $selected_block[$k]["listorder"];
                $enabledDesktop[$k]["position"] = $selected_block[$k]["position"];
            }
        }
        
//        print_r($enabledDesktop);exit;
        
        $this->assign("blocks", $enabledDesktop);
        $this->display();
    }
    
}

?>
