<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserAction
 *
 * @author 志鹏
 */
class UserAction extends CommonAction {
    
    public function index() {
        $model = D("UserRelation");
        $list = $model->relation(true)->select();
        $this->assign("list", $list);
        cookie('_currentUrl_', __SELF__);
        $this->display();
    }
    
    public function _before_add() {
        $id = abs(intval($_GET["id"]));
        $dept = D("Department");
        $tmp = $dept->getTree();
        foreach($tmp as $k=>$t) {
            $theDepts[$t["id"]] = $t["prefix"].$t["name"];
        }
        $this->assign("departments", $theDepts);
        
        //用户组
        $ag = D("AuthGroup");
        $allGroups = $ag->select();
        
        $aga = D("AuthGroupAccess");
        $groups = $aga->where("uid=".$id)->select();
        
        foreach($groups as $g) {
            $selectedGroup[] = $g["group_id"];
        }
        $this->assign("allGroups", $allGroups);
        $this->assign("seletedGroups", $selectedGroup);
    }
    
    public function _before_edit() {
        $id = abs(intval($_GET["id"]));
        $dept = D("Department");
        $tmp = $dept->getTree();
        foreach($tmp as $k=>$t) {
            $theDepts[$t["id"]] = $t["prefix"].$t["name"];
        }
        $this->assign("departments", $theDepts);
        
        //用户组
        $ag = D("AuthGroup");
        $allGroups = $ag->select();
        
        $aga = D("AuthGroupAccess");
        $groups = $aga->where("uid=".$id)->select();
        
        foreach($groups as $g) {
            $selectedGroup[] = $g["group_id"];
        }
        $this->assign("allGroups", $allGroups);
        $this->assign("seletedGroups", $selectedGroup);
    }
    
    public function _after_insert() {
        F("User/All", NULL);
    }
    
    public function _after_update() {
        F("User/All", NULL);
    }
    
    public function editGroup() {
        $groupids = $_POST["group_id"];
        $uid = abs(intval($_POST["id"]));
        $model = D("AuthGroupAccess");
        $model->where("uid=".$uid)->delete();
        if($groupids) {
            foreach($groupids as $gi) {
                if($gi <= 0) {
                    continue;
                }
                $model->add(array(
                    "uid" => $uid,
                    "group_id" => $gi
                ));
            }
        }
        
        $this->success(L("operate_success"));
    }
    
    /**
     * Ajax获取用户列表
     */
    public function ajax_getUsers() {
        $name = trim(strip_tags($_GET["term"]));
        $customer = D("User");
        $data = $customer->where(array(
            "truename" => array("LIKE", "%".$name."%")
        ))->select();
//        echo $customer->getLastSql();
        if(!$data) {
            $this->ajaxReturn(array());
        }
        
        foreach($data as $k=>$v) {
            $_data[] = array(
                "id" => $v["id"],
                "label" => $v["truename"],
                "value" => $v["truename"]
            );
        }
        return $this->ajaxReturn($_data);
    }
    
}

?>
