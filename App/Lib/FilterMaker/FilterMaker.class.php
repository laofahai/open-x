<?php

/**
 * @filename FilterMaker.class.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-14  10:27:14
 * @Description
 * 
 */
class FilterMaker {
    
    protected $filterStruct;
    
    private $theFieldsName = array(
        "filter_field", "filter_value"
    );
    
    public function __construct($model, $action="", $groupName = "") {
        $groupName = $groupName ? $groupName : GROUP_NAME;
        if($action) {
            $file = sprintf("%s/Conf/Filters/%s/%s%s.php", APP_PATH,$groupName, $model, $action);
        } else {
            $file = sprintf("%s/Conf/Filters/%s/%s.php", APP_PATH,$groupName, $model);
        }
        
        if(!file_exists_case($file)) {
            $file = sprintf("%s/Conf/Filters/%s.php", APP_PATH, $model);
        }
        
        if(file_exists_case($file)) {
            $key = md5($file);
            if(!key_exists($key, $this->requiredList)) {
                $this->requiredList[$key] = require $file;
            }
            $this->filterStruct = $this->requiredList[$key];
        }
    }
    
    public function make($data = array()) {
        if(!$this->filterStruct) {
            return false;
        }
        
        import("@.Form.Form");
        import("@.Form.FieldsInterface");
        import("@.Form.FieldsAbstract");
        $fieldsDefine = array();
        
        foreach($this->filterStruct as $k=>$v) {
            $method = sprintf("_%s", $k);
            if(method_exists($this, $method)) {
                switch($k) {
                    case "actions":
                        $return["actions"][] = $this->$method($v);
                        break;
                    case "primary":
                        $return["primary"] = $this->$method($v);
                    default:
                        $fieldsDefine[] = $this->$method($v);
                }
            }
        }
        $return["actions"][] = sprintf('<button type="submit" class="btn btn-primary">%s</button>', L("search"));
        
        if($_GET) {
            foreach($_GET as $k=>$v) {
                if(is_array($v) or in_array($k, $this->theFieldsName)) {
                    continue;
                }
                $fieldsDefine[] = sprintf('<input type="hidden" name="%s" value="%s" />', $k, $v);
            }
        }
        $return["fields"] = $fieldsDefine;
//        print_r($return);exit;
        return $return;
    }
    
    private function _daterange($options=array()) {
        $options["format"] = $options["format"] ? $options["format"] : "yyyy-mm-dd";
        $tpl = <<<EOF
                <div data-date="%s" data-date-format="%s" class="filter-item input-append date datepicker">
                    <input type="text" data-date="%s" data-date-format="%s" value="%s" class="span11" style="width:90px" name="%s" />
                    <span class="add-on"><i class="icon icon-calendar"></i> </span>
                </div>
EOF;
        $start = sprintf($tpl, $options["default_start"], $options["format"],$options["default_start"], $options["format"], $options["default_start"], "date_start");
        $end = sprintf($tpl, $options["default_start"], $options["format"],$options["default_end"], $options["format"], $options["default_end"], "date_end");
        
        $this->theFieldsName[] = "date_start";
        $this->theFieldsName[] = "date_end";
        
        return array(
            "label" => L("datetime"),
            "html"  => $start.$end
        );
    }
    
    private function _keyword($options = array()) {
        if(!$options["fields"]) {
            return;
        }
        
        $options["select_name"] = $options["select_name"] ? $options["select_name"] : "filter_field";
        $options["keyword_name"] = $options["keyword_name"] ? $options["keyword_name"] : "filter_value";
        
        foreach($options["fields"] as $k=>$v) {
            $selected = $_GET[$options["select_name"]] == $k ? " selected" : "";
            $opt[] = sprintf('<option value="%s"%s>%s</option>', $k, $selected, $v);
        }
        
        $select = sprintf('<select style="width:100px" name="%s" class="filter-item">%s</select>', $options["select_name"], implode("", $opt));
        
        $input = sprintf('<input type="text" name="%s" value="%s" class="filter-item span2 filter_value" />', $options["keyword_name"], $_GET[$options["keyword_name"]]);
        
        return array(
            "label" => L("keyword"),
            "html"  => $select.$input
        );
    }
    
    private function _actions($options = array()) {
        if(!$options) {
            return;
        }
        
        foreach($options as $k=>$v) {
            list($action, $class) = explode("|", $v);
            $theActions[] = sprintf('<a href="%s" class="btn btn-%s">%s</a>', $action, $class, L($k));
        }
        
        return implode(" ", $theActions);
    }
    
    private function _select($options = array()) {
        if(!$options) {
            return ;
        }
        foreach($options as $name=>$data) {
            $optionsHTML = array();
            foreach($data as $k=>$v) {
                $selected = $_GET[$name] == $k ? " selected" : "";
                $optionsHTML[] = sprintf('<option value="%s"%s>%s</option>', $k, $selected, $v);
            }
//            print_r($optionsHTML);
            $selects['childs'][] = array(
                "label" => L($name),
                "html"  => sprintf('<select class="filter-item" name="%s">%s</select>', $name, implode("", $optionsHTML))
            );
            $this->theFieldsName[] = $name;
        }
//        exit;
        return $selects;
    }
    
    private function _primary($options = array()) {
        if(!$options) {
            return;
        }
        foreach($options as $k=>$v) {
            $options[$k]["label"] = $v["label"] ? $v["label"] : $k;
        }
        return $options;
    }
    
}

?>
