<?php

/**
 * @filename JXC.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-8 10:49:35
 * @description
 * 
 */
return array(
    "express_from_name" => "发件人",
    "express_from_company" => "发件公司",
    "express_from_address" => "发件地址",
    "express_from_phone" => "发件人电话",
    "express_to_name" => "收件人",
    "express_to_company" => "收件公司",
    "express_to_address" => "收件地址",
    "express_tophone" => "收件人电话",
);