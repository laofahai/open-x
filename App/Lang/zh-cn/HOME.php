<?php

/**
 * @filename HOME.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-23 9:14:57
 * @description
 * 
 */
return array(
    "stockout_need_handle" => "出库待处理",
    "stockin_need_handle"  => "入库待处理",
    "my_desktop" => "我的桌面",
    "display" => "显示",
    "today_sale" => "今日销售",
    "set_permission" => "设置可执行权限",
    "new_message_department" => "部门群发",
    "refresh" => "刷新",
    "back" => "后退",
    "forward" => "前进"
);