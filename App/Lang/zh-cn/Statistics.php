<?php

/**
 * @filename Statistics.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-21 10:21:45
 * @description
 * 
 */

return array(
    "monthly_sale_chart" => "月度销售柱状图",
    "customer_rank" => "客户排行",
    "customer_yearly_chart" => "客户年度排行",
    "customer_monthly_chart"=> "客户月度排行",
    "saler_yearly_chart" => "销售年度排行",
    "saler_monthly_chart"=> "销售月度排行",
    "monthly_sale_bar" => "月度柱状图",
    "yearly_sale_bar"  => "年度柱状图",
    "saler_rank" => "销售排行",
    "monthly" => "月度",
    "yearly"  => "年度",
    "dayly" => "日度",
    "dayly_sale_bar" => "每日销售报表",
    "mulyearly_sale_bar" => "年度销售柱状图",
    "overview" => "总览",
    "purchase_price" => "采购金额",
    "purchase_amount"=> "采购数量",
    "sale_price" => "销售金额",
    "sale_amount"=> "销售数量"
);
?>
