<?php

/**
 * @filename desktop.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-23 15:01:43
 * @description
 * 
 */
return array(
    "NeedStockout" => array(
        "label" => "stockout_need_handle",
        "need"  => "JXC/Stockout",
        "num"   => 5,
        "link"  => "JXC/Stockout"
    ),
    "NeedStockin" => array(
        "label" => "stockin_need_handle",
        "need"  => "JXC/Stockin",
        "num"   => 5,
        "link"  => "JXC/Stockin"
    ),
    "NeedShipment" => array(
        "label" => "shipment_need_handle",
        "need"  => "JXC/Shipment",
        "num"   => 5,
        "link"  => "JXC/Shipment"
    ),
    "TodaySale" => array(
        "label" => "today_sale",
        "need"  => "JXC/Orders",
        "num"   => 5,
        "link"  => "JXC/OrdersDetail"
    )
);