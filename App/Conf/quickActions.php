<?php

/**
 * @filename quickActions.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-7 10:48:00
 * @description
 * 
 * action|icon|bg-color|span
 */

return array(
    "dashboard" => array(
        "add_new_order" => "JXC/Orders/add|success",
        "add_new_stockin" => "JXC/Stockin/add",
        "add_new_customer" => "CRM/RelationshipCompany/add",
        "stock_product_list" => "JXC/StockProductList",
        "stock_warning" => "JXC/Stock/warning|stockWarningNum",
        
        "__USED__" => array(
            "HOME/Index"
        )
    ),
    "basedata" => array(
        "goods_category" => "JXC/GoodsCategory",
        "goods" => "JXC/Goods",
        "stock_manage" => "JXC/Stock",
        "color" => "JXC/GoodsColor",
        "standard" => "JXC/GoodsStandard",
        "types_manage" => "HOME/Types",
        
        "__USED__" => array(
            "JXC/GoodsCategory",
            "JXC/Goods",
            "JXC/Stock",
            "JXC/GoodsColor",
            "JXC/GoodsStandard",
            "HOME/Types"
        )
    ),
    "org" => array(
        "member" => "HOME/User|user",
        "permission" => "HOME/AuthRule",
        "user_group" => "HOME/AuthGroup",
        "department" => "HOME/Department",
        
        "__USED__" => array(
            "HOME/User",
            "HOME/AuthRule",
            "HOME/AuthGroup",
            "HOME/Department"
        )
    ),
    "orders" => array(
        "add_new_order" => "JXC/Orders/add|success",
        "orders_list" => "JXC/Orders",
        "orders_detail_list" => "JXC/OrdersDetail",
        
        "__USED__" => array(
            "JXC/Orders"
        )
    ),
    "purchase" => array(
        "purchase_list" => "JXC/Purchase",
        "add_new_purchase_paper" => "JXC/Purchase/add",
        
        "__USED__" => array(
            "JXC/Purchase"
        )
    ),
    "finance" => array(
        "finance_record" => "Finance/FinanceRecord",
        "finance_receive_plan" => "Finance/FinanceReceivePlan",
        "finance_pay_plan" => "Finance/FinancePayPlan",
        "finance_receive_plan" => "Finance/FinanceReceivePlan",
        "fund_input" => "Finance/FinanceRecord/add/recordType/1",
        "fund_output" => "Finance/FinanceRecord/add/recordType/2",
        "finance_account" => "Finance/FinanceAccount",
        
        "__USED__" => array(
            "Finance/FinanceRecord",
            "Finance/FinanceAccount",
            "Finance/FinancePayPlan",
            "Finance/FinanceReceivePlan",
        )
    ),
    "accounting_voucher" => array(
        "add_new" => "Accounting/AccountingVoucher/add|success",
        "list"   => "Accounting/AccountingVoucher",
        
        "__USED__" => array(
            "Accounting/AccountingVoucher"
        )
    )
);

//"JXC/GoodsCategory" = array(
//    "add_new_order" => "asdasdf|asdfasd|asdfasdf"
//);