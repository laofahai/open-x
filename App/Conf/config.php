<?php
return array(
    'APP_GROUP_LIST' => 'HOME,CRM,SCM,JXC,OA,Finance,Statistics,Produce,OA,Accounting', //项目分组设定
    'DEFAULT_GROUP'  => 'HOME', //默认分组
    'URL_MODEL' => 2,
    
    // 已启用模块
    'ENABLED_MODULE' => array(
        "CRM",
        "Purchase",
        "Orders",
        "Stockin",
        "Stockout",
        "Shipment",
        "OA", 
        "Finance", 
        "Production",
        "Produce",
        "Accounting"
    ),
    
    'LOAD_EXT_CONFIG' => array(
        'BCN' => 'BreadcrumbNavigation', //用户配置
     ),
    
    'TAGLIB_PRE_LOAD' => 'html',
    
    'LANG_SWITCH_ON' => true,   // 开启语言包功能
//    'LANG_AUTO_DETECT' => true, // 自动侦测语言 开启多语言功能后有效
    'LANG_LIST'        => 'zh-cn,en-us', // 允许切换的语言列表 用逗号分隔
    'VAR_LANGUAGE'     => '0', // 默认语言切换变量
    
//    'TAGLIB_PRE_LOAD' => 'html',
    
    /* 常用配置 */
    'DB_TYPE'            =>    'mysql',        // 数据库类型
    'DB_HOST'            =>    'localhost',    // 数据库服务器地址
    'DB_NAME'            =>    'x',            // 数据库名
    'DB_USER'            =>    'root',        // 数据库用户名
    'DB_PWD'             =>    '1',    // 数据库密码
    'DB_PORT'            =>    3306,            // 数据库端口
    'DB_PREFIX'          =>    'x_',            // 数据库表前缀
    'DB_CHARSET'         =>    'utf8',            // 数据库编码
    'SECURE_CODE'        =>    'the_x',    // 数据加密密钥
    
    /**
     * 安全设置
     */
    'TOKEN_ON'=>false,  // 是否开启令牌验证 默认关闭
    'TOKEN_NAME'=>'__hash__',    // 令牌验证的表单隐藏字段名称
    'TOKEN_TYPE'=>'md5',  //令牌哈希验证规则 默认为MD5
    'TOKEN_RESET'=>true,  //令牌验证出错后是否重置令牌 默认为true
    'VAR_FILTERS'=>'filter_vars',
    
    /**
     * AUTH 权限控制
     */
    'AUTH_CONFIG'=>array(
        'AUTH_ON' => true, //认证开关
        'AUTH_TYPE' => 1, // 认证方式，1为时时认证；2为登录认证。
        'AUTH_GROUP' => 'x_auth_group', //用户组数据表名
        'AUTH_GROUP_ACCESS' => 'x_auth_group_access', //用户组明细表
        'AUTH_RULE' => 'x_auth_rule', //权限规则表
        'AUTH_USER' => 'x_user'//用户信息表
    ),
    
    'ENABLED_DESKTOP' => require "desktop.php",
    
);