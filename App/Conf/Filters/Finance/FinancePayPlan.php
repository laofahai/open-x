<?php

/**
 * @filename FinancePayPlan.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-27 10:12:09
 * @description
 * 
 */
return array(
    "select" => array(
        "type" => array(
            "1" => L("fund_input"),
            "2" => L("fund_output"),
        )
    ),
    "daterange" => array(
        "format" => "yyyy-mm-dd",
    ),
    "actions" => array(
        "view_all" => U("index")
    )
);
