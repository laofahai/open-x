<?php

/**
 * @filename ProductView.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-16 16:04:04
 * @description
 * 
 */
return array(
    "daterange" => array(
        "format" => "yyyy-mm-dd",
    ),
    "select" => array(
        "stock_id" => getStocks(true)
    ),
    "actions" => array(
        "view_all" => U("index")
    )
);
