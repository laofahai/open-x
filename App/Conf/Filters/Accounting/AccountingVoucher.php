<?php

/**
 * @filename AccountingVoucher.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2014-1-10 14:25:10
 * @description
 * 
 */
return array(
    "keyword" => array(
        "fields" => array(
            "summary" => L("summary")
        )
    ),
    "daterange" => array(
        "format" => "yyyy-mm-dd",
    ),
    "actions" => array(
        "view_all" => U("index")
    )
);
