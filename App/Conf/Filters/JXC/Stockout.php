<?php

/**
 * @filename Stockin.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-13 14:53:10
 * @description
 * 
 */

return array(
    "select" => array(
        "stock_id" => getStocks(true),
        "workflow_node" => getWorkflowNodesStatus("stockout", true),
    ),
    "keyword" => array(
        "fields" => array(
            "subject" => L("subject")
        )
    ),
    "daterange" => array(
        "format" => "yyyy-mm-dd",
    ),
    "actions" => array(
        "view_all" => U("index")
    )
);
