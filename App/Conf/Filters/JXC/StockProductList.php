<?php

/**
 * @filename StockProductList.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-14 15:15:30
 * @description
 * 
 */
return array(
    "select" => array(
        "stock_id" => getStocks(true)
    ),
    "keyword" => array(
        "fields" => array(
            "factory_code_all" => L("factory_code_all"),
            "goods_name" => L("goods_name")
        )
    ),
    "actions" => array(
        "view_all" => U("index")
    )
);
