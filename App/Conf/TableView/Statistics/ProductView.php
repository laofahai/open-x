<?php

/**
 * @filename ProductView.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-16 16:06:10
 * @description
 * 
 */
return array(
    "fields" => array(
        "id", 
        "factory_code_all", 
        "goods_name",
        "standard_name", 
        "color_name", 
        "purchase_amount",
        "purchase_price", 
        "sale_amount", 
        "sale_price", 
        "store_num"
    ),
    "actions"=> false,
    "pages" => false,
    
    "tableAttr" => array(
        "class" => "data-table"
    ),
    "useDefaultTheme" => true,
    "sortable" => true
);
