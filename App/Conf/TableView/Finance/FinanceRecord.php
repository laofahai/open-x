<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
    "fields" => array(
        "id", 
        "account_id" => array(
            "decorate" => "toAccountName"
        ), 
        "dateline" => array(
            "decorate" => "toDate"
        ), 
        "amount",
        "type" => array(
            "decorate" => "toFinanceRecordType"
        ),
        "memo"
    ),
    "actions"=> array(
        'view_detail' => "javascript:viewDetail(%d)|list", 
        "delete" => "javascript:del(%d)|trash",
//        "delete" => "javascript:del(%d)|remove",
//        "disable" => "javascript:changeStatus(%d,0)|lock|status=1",
//        "enable" => "javascript:changeStatus(%d,1)|unlock|status=0",
    ),
    "pages" => true,
    
    "tableAttr" => array(),
    "useDefaultTheme" => true,
    "sortable" => true,
);
