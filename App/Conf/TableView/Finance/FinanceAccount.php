<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


return array(
    "fields" => array(
        "id", "name", "listorder","balance"
    ),
    "actions"=> array(
        'edit' => "javascript:edit(%d)|pencil", 
        "fund_input" => "/Finance/FinanceRecord/add/recordType/1/id/%d|plus-sign",
        "fund_output" => "/Finance/FinanceRecord/add/recordType/2/id/%d|minus-sign",
        "view_detail" => "/Finance/FinanceRecord/index/account_id/%d|list"
//        "delete" => "javascript:del(%d)|remove",
//        "disable" => "javascript:changeStatus(%d,0)|lock|status=1",
//        "enable" => "javascript:changeStatus(%d,1)|unlock|status=0",
    ),
//    "pages" => true,
    
    "tableAttr" => array(),
    "useDefaultTheme" => true,
    "sortable" => true,
    "sortAlias"=> array(
        "status_lang" => "status"
    )
);
