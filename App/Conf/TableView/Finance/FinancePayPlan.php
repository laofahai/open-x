<?php

/**
 * @filename Shipment.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-6 11:30:55
 * @description
 * 
 */
return array(
    "fields" => array(
        "id",
        "subject", 
        "account_id" => array(
            "decorate" => "toAccountName"
        ), 
        "supplier_id" => array(
            "label" => "supplier",
            "decorate" => "toSupplierName",
            "link" => "/CRM/RelationshipCompany/viewDetail/id/%d"
        ), 
        "create_dateline" => array(
            "decorate" => "toDate"
        ), 
        "pay_dateline" => array(
            "label" => "process_dateline",
            "decorate" => "toDate"
        ), 
        "user_id" => array(
            "label" => "saler",
            "decorate" => "toTruename"
        ), 
        "financer_id" => array(
            "label" => "finance",
            "decorate" => "toTruename"
        ), 
    ),
    "actions"=> array(),
    "pages" => true,
    
    "tableAttr" => array(),
    "useDefaultTheme" => true,
    "useDefaultAction"=> false,
    "sortable" => true,
);

