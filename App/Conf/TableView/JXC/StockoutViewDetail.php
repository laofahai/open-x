<?php

/**
 * @filename OrdersViewDetail.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-26 10:19:59
 * @description
 * 
 */
return array(
    "fields" => array(
        "id",
        "goods_name", 
        "color_name",
        "standard_name",
        "per_price",
        "price",
        "num" => array(
            "fullParams" => true, //使用整条数据作为修饰的参数 需要定义于decorate之前
            "decorate" => "displayCheckNumHTML",
        ),
        "store_num",
        "measure",
    ),
    "actions"=> false,
    "pages" => false,
    "useDefaultTheme" => true,
    "sortable" => true,
);