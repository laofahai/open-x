<?php

/**
 * @filename Shipment.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-6 11:30:55
 * @description
 * 
 */
return array(
    "fields" => array(
        "id", "from_name", "from_phone", "to_name", "to_phone", "weight", "total_num", "freight", 
        "freight_type" => array(
            "decorate" => "toTypeName"
        )
    ),
    "actions"=> array(
        'edit' => "javascript:edit(%d)|pencil", 
        "delete" => "javascript:del(%d)|remove",
        "view_detail" => "javascript:viewDetail(%d)|list"
    ),
    "pages" => true,
    
    "tableAttr" => array(),
    "useDefaultTheme" => true,
    "sortable" => true,
);

