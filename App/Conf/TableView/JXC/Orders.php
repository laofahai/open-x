<?php

/**
 * @filename Shipment.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-6 11:30:55
 * @description
 * 
 */
return array(
    "fields" => array(
        "id",
        "subject", 
        "saler_id" => array(
            "decorate" => "toTruename"
        ), 
        "sale_type" => array(
            "decorate" => "toTypeName"
        ), 
        "customer_id" => array(
            "label" => "customer",
            "decorate" => "toCustomerName",
            "link" => "/CRM/RelationshipCompany/viewDetail/id/%d"
        ), 
        "total_num", 
        "total_price", 
        "total_price_real", 
        "dateline" => array(
            "decorate" => "toDate"
        ), 
        "status"
    ),
    "actions"=> array(),
    "pages" => true,
    
    "tableAttr" => array(),
    "useDefaultTheme" => true,
    "sortable" => true,
);

