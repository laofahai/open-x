<?php

/**
 * @filename StockProductList.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-23 10:29:06
 * @description
 * 
 */
return array(
    "actions" => false,
    "fields"  => array(
        "id", "factory_code_all", 
        "goods_name",
        "color_name",
        "stock_id" => array(
            "decorate" => "toStockName",
            "label" => "stock"
        ),
        "standard_name",
        "num",
        "store_min"
    ),
    "pages" => true,
    "sortable" => true
);