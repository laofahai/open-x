<?php

/**
 * @filename OrdersViewDetail.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-26 10:19:59
 * @description
 * 
 */
return array(
    "fields" => array(
        "id",
        "factory_code_all",
        "goods_name", 
        "color_name",
        "standard_name",
        "num",
//        "store_num",
        "measure",
    ),
    "actions"=> false,
    "pages" => false,
    "useDefaultTheme" => true,
    "sortable" => true,
);