<?php

/**
 * @filename _default.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-5 15:43:39
 * @description
 * 
 */
return array(
    "fields" => array(
        "id", "name", "listorder"
    ),
    "actions"=> array(
        'edit' => "javascript:edit(%d)|pencil", 
        "delete" => "javascript:del(%d)|remove"
    ),
    "pages" => true,
    
    "tableAttr" => array(),
    "useDefaultTheme" => true,
    "sortable" => true,
);
