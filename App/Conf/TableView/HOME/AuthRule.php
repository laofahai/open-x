<?php

/**
 * @filename AuthRule.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-5 16:50:46
 * @description
 * 
 */
return array(
    "fields" => array(
        "id", "name", "title","status_lang","category","condition"
    ),
    "actions"=> array(
        'edit' => "javascript:edit(%d)|pencil", 
        "delete" => "javascript:del(%d)|remove",
        "disable" => "javascript:changeStatus(%d,0)|lock|status=1",
        "enable" => "javascript:changeStatus(%d,1)|unlock|status=0",
    ),
    "pages" => true,
    
    "tableAttr" => array(),
    "useDefaultTheme" => true,
    "sortable" => true,
);
