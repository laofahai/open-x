<?php

/**
 * @filename Department.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-5 16:07:19
 * @description
 * 
 */
return array(
    "fields" => array(
        "id", "prefix_name",
    ),
    "actions"=> array(
        "add_child" => "javascript:addChild(%d)|plus",
        'edit' => "javascript:edit(%d)|pencil", 
        "delete" => "javascript:del(%d)|remove"
    ),
    "pages" => false,
    
    "tableAttr" => array(),
    "useDefaultTheme" => true,
    "sortable" => false,
);

