<?php

/**
 * @filename Types.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-7 10:04:04
 * @description
 * 
 */
return array(
    "fields" => array(
        "id", "name", "alias", "type" => array(
            "decorate" => "toTypesCategory"
        ),"listorder"
    ),
    "actions"=> array(
        'edit' => "javascript:edit(%d)|pencil", 
        "delete" => "javascript:del(%d)|remove",
        "disable" => "javascript:changeStatus(%d,0)|lock|status=1",
        "enable" => "javascript:changeStatus(%d,1)|unlock|status=0",
    ),
    "pages" => true,
    
    "tableAttr" => array(),
    "useDefaultTheme" => true,
    "sortable" => true,
);