<?php

/**
 * 面包屑导航
 */
return array(
    "Home" => array(
        
    ),
    "JXC" => array(
        "url"  => "#nogo",
        "name" => "进销存",
        "child"=> array(
            "StockProductList" => array(
                "name" => "库存清单"
            ),
            "Stockin" => array(
                "name" => "入库单",
                "child"=> array(
                    "add" => "新建入库单",
                    "edit"=> "编辑入库单",
                    "editDetail" => "修改明细",
                    "viewDetail" => "查看明细"
                )
            )
        )
    )
);