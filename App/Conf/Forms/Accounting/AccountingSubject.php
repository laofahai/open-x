<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
    "fields" => array(
        "parent_node" => array(
            "type" => "staticText",
            "value"=> toFinanceAccountingSubject($_GET["parentId"]),
            "hoe"  => true
        ),
        "code",
        "direction" => array(
            "type" => "select",
            "data-source" => array(
                "借", "贷"
            )
        ),
        "category" => array(
            "type" => "select",
            "data-source" => getFinanceAccountingSubjectCategory()
        ),
        "name",
        "listorder" => array(
            "type" => "text",
            "value"=> "99"
        ),
        "is_assist" => array(
            "type" => "checkbox",
            "data-source" => array(
                "1" => L("is_assist"),
            ),
        ),
        "assist_items[]" => array(
            "label"=> "assist_items",
            "type" => "checkbox",
            "data-source" => array(
                "custom" => L("customer"),
                "supplier" => L("supplier"),
                "emp" => L("member"),
                "dept" => L("department"),
            ),
        ),
        "pid" => array(
            "value" => $_GET["parentId"],
            "type"  => "hidden"
        )
    )
);