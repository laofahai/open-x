<?php

/**
 * @filename WorkflowMemo.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-25 15:58:35
 * @description
 * 
 */
return array(
    "fields" => array(
        "memo" => array(
            "name" => "memo",
            "label"=> "memo",
            "type" => "textarea",
            "class"=> "span7",
            "style"=> "height:80px;",
        )
    )
);
?>
