<?php

/**
 * @filename PassportProfile.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-13 14:27:13
 * @description
 * 
 */

return array(
    "fields" => array(
        "username" => array(
            "disabled" => "disabled"
        ),
        "email" => array(
            "disabled" => "disabled"
        ),
        "truename",
        "password" => array(
            "required" => false,
            "placeholder" => L("set empty if do not change")
        ),
        "phone" => array(
            "label" => "cellphone"
        )
    ),
    "action" => "__URL__/Profile",
);
?>
