<?php

/**
 * @filename addStock.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-23 11:39:23
 * @description
 * 
 */

return array(
    "fields" => array(
        "name" => array(
            "label" => "action"
        ),
        "title",
        "condition" => array(
            "required" => false
        ),
        "category" => array(
            "value" => "all"
        )
//        "email" => array(
//            "type" => "email",
//        ),
//        "select" => array(
//            "type" => "select",
//            "data-source" => getStocks()
//        ),
//        "radio" => array(
//            "name" => "radio",
//            "type" => "radio",
//            "data-source" => getStocks()
//        ),
//        "checkbox" => array(
//            "name" => "check[]",
//            "type" => "checkbox",
//            "data-source" => getStocks()
//        ),
//        "file" => array(
//            "name" => "file",
//            "type" => "file"
//        )
    )
);

?>
