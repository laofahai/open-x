<?php

/**
 * @filename Workflow.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-6 15:59:41
 * @description
 * 
 */
return array(
    "fields" => array(
        "name",
        "alias",
        "workflow_file",
        "memo" => array(
            "required" => false
        )
    )
);
