<?php

/**
 * @filename Types.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-7 10:02:40
 * @description
 * 
 */
return array(
    "fields" => array(
        "name",
        "type" => array(
            "type" => "select",
            "data-source" => getTypesCategory()
        ), 
        "alias" => array(
            "required" => false
        ),
        "listorder" => array("value"=>"99")
    )
    
);