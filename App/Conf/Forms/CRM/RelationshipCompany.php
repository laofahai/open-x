<?php

/**
 * @filename Customer.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-26 11:37:30
 * @description
 * 
 */

return array(
    "fields" => array(
        "name",
        "contact",
        "phone",
        "email" => array(
            "type" => "email"
        ),
        "region_id" => array(
            "label"=> "region",
            "type" => "RegionSelect"
        ),
        "address",
        "status" => array(
            "type" => "radio",
            "data-source" => array(
                L("private"), L("public")
            ),
            "checked" => "1",
            "hoe" => true,
        ),
        "insert_linkman" => array(
            "label" => "",
            "type" => "checkbox",
            "data-source" => array(
                "1" => L("insert_linkman_sametime")
            ),
            "checked" => "1",
            "hoe" => true,
        ),
        "is_customer" => array(
            "label" => "type",
            "type" => "checkbox",
            "data-source" => array(
                "1" => L("is_customer"),
            ),
            "checked" => "1",
        ),
        "is_supplier" => array(
            "label" => "type",
            "type" => "checkbox",
            "data-source" => array(
                "1" => L("is_supplier"),
            ),
            "checked" => "1",
        )
    )
);
?>
