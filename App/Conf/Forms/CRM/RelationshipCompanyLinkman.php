<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
    "fields" => array(
        "contact","phone1",
        "phone2" => array(
            "required" => false
        ),
        "email"=>array(
            "type" => "email"
        ),
        "qq" => array(
            "required" => false
        ),
        "extra" => array(
            "required" => false
        ),
        "relationship_company_id" => array(
            "type" => "hidden",
            "value"=> $_GET["relationship_company_id"]
        )
    )
);