<?php

/**
 * @filename RelationshipCompany.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-26 11:37:30
 * @description
 * 
 */

return array(
    "fields" => array(
        "name",
        "contact",
        "phone",
        "address",
        "status" => array(
            "type" => "hidden",
            "value"=> 1
        ),
        "insert_linkman" => array(
            "type" => "hidden",
            "value" => "1",
        ),
    )
);
?>
