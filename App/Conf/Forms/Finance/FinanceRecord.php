<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
    "fields" => array(
        "record_type" => array(
            "type" => "StaticText",
            "value"=> toFinanceRecordType($_GET["recordType"])
        ),
        "account_id" => array(
            "type" => "select",
            "data-source" => getFinanceAccount(),
            "selected" => $_GET["id"]
        ),
        "amount",
        "type_id" => array(
            "type" => "hidden",
            "value"=> $_GET["recordType"] == 2 ? $_GET["recordType"] : 1
        ),
        "type" => array(
            "type" => "select",
            "data-source" => getTypesIndex($_GET["recordType"] == 2 ? "pay" : "receive")
        ),
        "memo" => array(
            "type" => "textarea"
        )
    )
);
