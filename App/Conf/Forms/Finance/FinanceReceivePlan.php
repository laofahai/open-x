<?php

/**
 * @filename FinancePayPlan.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-8 8:56:47
 * @description
 * 
 */
return array(
    "fields" => array(
        "subject",
        "customer" => array(
            "type" => "RelationshipCompanySelect",
            "autocomplete-source" => U("/CRM/RelationshipCompany/ajax_getCompanyss"),
            "minLength" => 1,
            "callback" => "afterSupplierAutoSelect",
            "goto" => U("/Finance/FinancePayPlan/add"),
            "hideField" => "supplier_id",
            "value" => $_GET["rel_company_name"]
        ),
        "customer_id" => array(
            "type" => "hidden",
            "hoe" => true,
            "id"  => "supplier_id"
        ),
        "type_id" => array(
            "type" => "select",
            "data-source" => getTypesIndex("receive")
        ),
        "amount",
        "memo" => array(
            "type" => "textarea"
        )
    ),
);
