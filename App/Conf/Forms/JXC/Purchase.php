<?php

/**
 * @filename Purchase.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-4 11:21:09
 * @description
 * 
 */
return array(
    "fields" => array(
        "subject",
        "supplier" => array(
            "type" => "RelationshipCompanySelect",
            "autocomplete-source" => U("/CRM/RelationshipCompany/ajax_getCompanyss"),
            "minLength" => 1,
            "callback" => "afterSupplierAutoSelect",
            "goto" => U("/JXC/Purchase/add"),
            "hideField" => "supplier_id",
            "value" => $_GET["rel_company_name"]
        ),
        "supplier_id" => array(
            "type" => "hidden",
            "hoe" => true,
            "id"  => "rel_company_id"
        ),
        "stock_id" => array(
            "label" => "stockin_stock",
            "type" => "select",
            "data-source" => getStocks()
        ),
        "purchase_type" => array(
            "label" => "purchase_type",
            "type" => "select",
            "data-source" => getTypesIndex("purchase")
        ),
        "memo" => array(
            "type" => "textarea",
            "style"=> "height:80px;",
            "required" => false,
            "class"=> "span8"
        ),
    )
);

?>
