<?php

/**
 * @filename Orders.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-4 11:21:09
 * @description
 * 
 */
return array(
    "fields" => array(
        "subject",
        "customer" => array(
            "type" => "RelationshipCompanySelect",
        ),
        "customer_id" => array(
            "type" => "hidden",
            "hoe" => true,
            "id"  => "rel_company_id"
        ),
        "stock_id" => array(
            "label" => "stockout_stock",
            "type" => "select",
            "data-source" => getStocks()
        ),
        "sale_type" => array(
            "label" => "sale_type",
            "type" => "select",
            "data-source" => getTypesIndex("sale")
        ),
        "memo" => array(
            "type" => "textarea",
            "style"=> "height:80px;",
            "required" => false,
            "class"=> "span8"
        ),
    )
);

?>
