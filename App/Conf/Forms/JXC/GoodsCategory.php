<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
    "fields" => array(
        "parent_node" => array(
            "type" => "staticText",
            "value"=> toGoodsCategoryName($_GET["parentId"]),
            "hoe"  => true
        ),
        "name",
        "listorder" => array(
            "type" => "text",
            "value"=> "99"
        ),
        "pid" => array(
            "value" => $_GET["parentId"],
            "type"  => "hidden"
        )
    )
);