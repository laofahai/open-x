<?php

/**
 * @filename Shipment.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-12-6 10:52:08
 * @description
 * 
 */
return array(
    "fields" => array(
        "from_name" => array(
            "label" => "express_from_name",
        ),
        "from_company" => array(
            "label" => "express_from_company",
            "value" => DBC("company_name")
        ),
        "from_address" => array(
            "label" => "express_from_address",
            "value" => DBC("company_address")
        ),
        "from_phone" => array(
            "label" => "express_from_phone",
            "value" => DBC("company_phone")
        ),
        "to_name" => array(
            "label" => "express_to_name",
        ),
        "to_company" => array(
            "label" => "express_to_company",
        ),
        "to_address" => array(
            "label" => "express_to_address",
        ),
        "to_phone" => array(
            "label" => "express_tophone",
        ),
        "shipment_type" => array(
            "type" => "select",
            "data-source" => getTypesIndex("shipment")
        ),
        "weight" => array(
            "add-on-after" => "kg",
            "value" => 0
        ),
        "freight_type" => array(
            "type" => "select",
            "data-source" => getTypesIndex("freight")
        ),
        "freight" => array(
            "value" => 0
        ),
        "total_num",
        "stockout_id" => array(
            "type" => "hidden",
            "value"=> 0
        )
    )
);