<?php

/**
 * @filename addStock.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-23 11:39:23
 * @description
 * 
 */

return array(
    "fields" => array(
        "name",
        "num" => array(
            "type" => "number",
            "hoe"=> true, //hide on edit
        ),
        "managers" => array(
            "label" => "stock_manager",
            "type" => "Select",
            "data-source" => getUserTruenameArray(),
            "multiple" => "multiple",
            "style" => "width:250px"
        ),
//        "editor" => array(
//            "type" => "editor"
//        )
//        "id" => array(
//            "type" => "hidden"
//        ),
//        "stock_id" => array(
//            "label" => "cacaca",
//            "type" => "radio",
//            "model"=> "Stock",
//            "condition" => "",
//            "order" => ""
//        )
//        "email" => array(
//            "type" => "email",
//        ),
//        "select" => array(
//            "type" => "select",
//            "data-source" => getStocks()
//        ),
//        "radio" => array(
//            "name" => "radio",
//            "type" => "radio",
//            "data-source" => getStocks()
//        ),
//        "checkbox" => array(
//            "name" => "check[]",
//            "type" => "checkbox",
//            "data-source" => getStocks()
//        ),
//        "file" => array(
//            "name" => "file",
//            "type" => "file"
//        )
    )
);

?>
