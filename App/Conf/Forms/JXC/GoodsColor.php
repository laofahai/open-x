<?php

/**
 * @filename GoodsColor.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-24 11:51:34
 * @description
 * 
 */

return array(
    "fields" => array(
        "name",
    )
);
?>