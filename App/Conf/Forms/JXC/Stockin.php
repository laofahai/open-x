<?php

/**
 * @filename addStock.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-23 11:39:23
 * @description
 * 
 */

return array(
    "fields" => array(
        "subject",
        "stock_id" => array(
            "label" => "stockin_stock",
            "type" => "select",
            "data-source" => getStocks()
        ),
        "memo" => array(
            "type" => "textarea",
            "style"=> "height:80px;",
            "required" => false,
            "class"=> "span8"
        ),
//        "customer" => array(
//            "type" => "RelationshipCompanySelect",
//            "autocomplete-source" => U("/CRM/RelationshipCompany/ajax_getCompanys"),
//            "minLength" => 1,
//            "callback" => "afterCustomerAutoSelect",
//            "goto" => U("/JXC/Orders/add"),
//            "hideField" => "customer_id",
//            "value" => $_GET["rel_company_name"]
//        ),
//        "customer_id" => array(
//            "type" => "hidden",
//            "hoe" => true,
//            "id"  => "customer_id"
//        )
//        "email" => array(
//            "type" => "email",
//        ),
//        "select" => array(
//            "type" => "select",
//            "data-source" => getStocks()
//        ),
//        "radio" => array(
//            "name" => "radio",
//            "type" => "radio",
//            "data-source" => getStocks()
//        ),
//        "checkbox" => array(
//            "name" => "check[]",
//            "type" => "checkbox",
//            "data-source" => getStocks()
//        ),
//        "file" => array(
//            "name" => "file",
//            "type" => "file"
//        )
    )
);

?>
