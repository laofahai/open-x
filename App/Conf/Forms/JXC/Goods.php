<?php

/**
 * @filename Goods.php 
 * @encoding UTF-8 
 * @author nemo.xiaolan <a href="mailto:335454250@qq.com">335454250@qq.com</a>
 * @link <a href="http://www.sep-v.com">http://www.sep-v.com</a>
 * @license http://www.sep-v.com/code-license
 * @datetime 2013-11-24 11:07:48
 * @description
 * 
 */
return array(
    "fields" => array(
        "goods_category_id" => array(
            "label" => "category",
            "type" => "select",
            "data-source" => getCategoryTree("GoodsCategory")
        ),
        "name", 
        "measure" => array(
            "value" => "件"
        ),
        "price",
        "factory_code",
        "store_min" => array(
            "number"
        )
    )
);