<?php

define("CTS", time()); // Current Timestamp
define("DS", DIRECTORY_SEPARATOR);
define("ENTRY_PATH", dirname(__FILE__));
define("APP_NAME", "ERP");
define("APP_PATH", ENTRY_PATH.DS."App/");

define("APP_DEBUG", true);


require ENTRY_PATH.DS.'TP'.DS.'ThinkPHP.php';