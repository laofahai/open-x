# Open-X 开源ERP系统

本项目已重构，转移至：http://git.oschina.net/xiaolan/ones

## 关于open-x

open-x基于PHP，目标是打造可解决实际问题优秀的B/S开源ERP系统，目前已实现往来关系管理、进销存管理、财务管理等功能。
项目使用ThinkPHP框架。基于可定制的工作流程，可根据不同客户需要灵活改编。
