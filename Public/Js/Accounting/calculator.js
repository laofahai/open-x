(function($){
    $.fn.calculator = function(options) {
        var defaults={
			leftX : 10,
			leftY : -2,
			nodeId :"#jsq",
			decimal: 2
		};
        var opts = $.extend({},defaults,options);

        $(this).each(function(){
			$(this).keydown(function(e) {
				var $this = $(this);
				//alert(e.which)
				if(e.which == 118){
					e.preventDefault();
					popcal($this,opts.leftX,opts.leftY,opts.nodeId,opts.decimal);
				}
				$this = null;
			});
        });
    };
	
	var popcal = function(obj,x,y,id,dec){
		  var offset = obj.offset();
		  var jsq = '<table class="jsq" id="jsq"><tr><th colspan="5"><input type="text" name="input" id="formula" /></th></tr><tbody id="jsq_com"><tr><td><input type="button" name="seven" value="7"></td><td><input type="button" name="eight" value="8"></td><td><input type="button" name="nine"  value="9"></td><td colspan="2"><input type="button" name="clear" value="C/清除" class="clear"></td></tr><tr><td><input type="button" name="four"  value="4"></td><td><input type="button" name="five"  value="5"></td><td><input type="button" name="six" value="6"></td><td><input type="button" name="times" value="*"></td><td><input type="button" name="div" value="/"></td></tr><tr><td><input type="button" name="one"   value="1"></td><td><input type="button" name="two"   value="2"></td><td><input type="button" name="three" value="3"></td><td><input type="button" name="plus"  value="+"></td><td><input type="button" name="minus" value="-"></td></tr><tr><td><input type="button" name="zero"  value="0"></td><td><input type="button" name="point" value="."></td><td colspan="3"><input type="button" name="DoIt" value="=" class="equal"></td></tr></tbody></table>';
		  if($("#jsq").length == 0){
			  $("body").append(jsq);	//把它追加到文档中
			 
			 var formula = $("#formula");
			  $("#jsq_com").delegate("input", "click", function(e){
			  	e.stopPropagation();
				var sResult = formula.val();
				switch(this.name){
					 case "DoIt":
					   try { 
						  formula.val(eval(sResult).toFixed(dec)); 
						  obj.val(formula.val()).focus(); 
						  $("#jsq").remove();
					   } catch(e){
						  $("#jsq").remove();
						  obj.focus(); 
						  //alert("输入了错误的算式...！");
						  //formula.select();
					   }
					   break;
					 case "clear":
					   formula.val("").focus();
					   break;
					 default:
					   formula.val(sResult+this.value)
					}
			  });
			  formula.keydown(function(e){
				if(e.which == 13) {
					try { 
					  formula.val(eval(formula.val()).toFixed(dec)); 
					  obj.val(formula.val()).focus(); 
					  $("#jsq").remove();
					} catch(e){
				      $("#jsq").remove();
					  obj.focus();
					  //alert("输入了错误的算式...！");
					  //formula.select();
					}
				} else if(!((e.which>=48&&e.which<=57)||(e.which>=96&&e.which<=111)||(e.which>32&&e.which<41)||e.which==8||e.which==46)){
					return false;
				}	
			  });
			  $(document).click(function(event){
				  if(!($(event.target).isChildAndSelfOf(id)||$(event.target).isChildAndSelfOf("#jsq"))){
				  	 $("#jsq").remove(); 
				  }
			  });
		  }
		  $("#jsq")
			  .css({
				  "top": (offset.top + y) + "px",
				  "left": (offset.left) + "px"
			  }).show();	  //设置x坐标和y坐标，并且显示
		  $("#formula").focus();	
	}
})(jQuery);