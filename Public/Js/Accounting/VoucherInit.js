var thisPage = {
    init: function() {
//		this.initDom();
        this.initEvent();
    },
    initEvent: function() {//事件绑定
        Voucher.initEvent();
    },
    handle: {
        clear: function clear() {
            $("#vch_attach").val(0);
            $("#capAmount").text('');
            $("#debit_total").text('').data('realValue', '');
            $("#credit_total").text('').data('realValue', '');
            //重置
            Voucher.resetTable();
            PAGE.vchID = "";
        },
        save: function() { //保存
            function successCallback(data) {
                if (data.status === 200) {
                    Public.tips({content: '保存凭证成功！'});
                    PAGE.edit = true;
                    PAGE.vchID = data.data.id;
                    $('#toolTop .left').html('<a class="ui-btn ui-btn-sp" id="add">新增</a><a class="ui-btn" id="edit">保存</a><a class="ui-btn"  id="print">打印</a><a class="ui-btn" id="audit">审核</a><a class="ui-btn" id="copy">复制</a><a class="ui-btn" id="delete">删除</a>');
                    $('#toolBottom').html('<div class="t-inner"><a class="ui-btn ui-btn-sp" id="editB">保存</a><a class="ui-btn" id="printB">打印</a>');
                    if (NEW_VCHID_LEN > 0) {
                        $("#prev").removeClass("ui-btn-prev-dis");
                    }
                    NEW_VCHID.push(data.data.id);
                    if (data.data.yearPeriod > Number(parent.SYSTEM.CURPERIOD)) {
                        parent.getPeriod();
                    }
                    ORIGIN_VCH_DATA = Voucher.getVoucherData();
                    return true;
                } else {
                    Public.tips({type: 1, content: data.msg});
                    return false;
                }
            }
            Voucher._addHandle('save', _APP_ + '/Accounting/AccountingVoucher/insert', successCallback);
        },
        saveAndNew: function() { //保存并新增		
            function successCallback(data) {
                if (data.status === 200) {
                    Public.tips({content: '保存凭证成功！'});
                    Voucher.curEditField.val('');
                    thisPage.handle.clear();
                    PAGE.edit = false;
                    PAGE.vchID = -1;
                    $("#vch_num").val(data.data.num); //重设凭证号
                    Voucher.firstFocus();
                    //记录本次操作凭证
                    $("#prev").removeClass("ui-btn-prev-dis");
                    NEW_VCHID.push(data.data.id);
                    NEW_VCHID_LEN = NEW_VCHID.length;
                    NEW_VCHID_POS = NEW_VCHID_LEN;
                    //更新有效会计期间
                    if (data.data.yearPeriod > Number(parent.SYSTEM.CURPERIOD)) {
                        parent.getPeriod();
                    }
                    ORIGIN_VCH_DATA = {};
                } else if (data.status === 900) {
                    $.dialog({
                        lock: true,
                        icon: 'alert.gif',
                        title: '系统提醒',
                        content: '您已录入300张以上的凭证，达到免费版上限， <br/>请购买 不受限制的 正式版 。',
                        button: [
                            {
                                name: '立即购买',
                                callback: function() {
                                    window.open('http://kuaiji.youshang.com/buy.jsp');
                                },
                                focus: true
                            },
                            {
                                name: '取消'
                            }
                        ]

                    });
                    return false;
                } else {
                    Public.tips({type: 1, content: data.msg});
                    return false;
                }
            }
            ;
            Voucher._addHandle('new', '/gl/voucher?m=saveAndNew', successCallback);
        },
        edit: function() { //修改保存	
            function successCallback(data) {
                if (data.status == 200) {
                    Public.tips({content: '修改凭证成功！'});
                    PAGE.edit = true;
                    PAGE.vchID = data.data.id;
                    if (data.data.yearPeriod !== Number(VCH_DATA.yearPeriod)) {
                        parent.getPeriod();
                    }
                    ;
                    VCH_DATA = data.data;
                    ORIGIN_VCH_DATA = Voucher.getVoucherData();
                }
                else {
                    Public.tips({type: 1, content: data.msg});
                }
            }
            Voucher._addHandle('edit', '/gl/voucher?m=update', successCallback);
        },
        saveTemp: function() { //保存模板
        },
        add: function() {
            if (PAGE.VIEW) {
                var url = '/voucher/voucher.jsp';
                parent.tab.addTabItem({tabid: 'voucher', text: '录凭证', url: url});
            } else {
                parent.tab.reload("voucher");
            }
        },
        copy: function() {
            if (PAGE.VIEW) {
                //查看界面的复制
                function handle() {
                    var voucherDoc = window.parent.document.getElementById("voucher").contentWindow;
                    if (voucherDoc.Voucher.curEditCell) {
                        voucherDoc.Voucher.cancelEdit(voucherDoc.Voucher.curEditCell);
                    }
                    voucherDoc.initEntry(VCH_DATA.entries);
                }
                ;
                if (parent.tab.isTabItemExist('voucher')) {
                    parent.tab.reload("voucher", handle);
                    parent.tab.selectTabItem("voucher");
                } else {
                    parent.tab.addTabItem({tabid: 'voucher', text: '录凭证', url: '/voucher/voucher.jsp', callback: handle});
                }

            } else {
                //录入界面的复制
                PAGE.vchID = "";
                $('#toolTop').html('<div class="left"><a class="ui-btn ui-btn-sp" id="renew">保存并新增</a><a class="ui-btn" id="save">保存</a></div>');

                Voucher.setNum($('#vch_mark').getCombo().getValue());
                //$("#vch_num").val(parseInt($("#vch_num").val()) + 1); //重设凭证号

                if ($("#tag").hasClass("has-audit")) {
                    $("#tag").removeClass();
                    vchEnable();
                    Voucher.initEvent();
                }
            }
        },
        remove: function() {
            $.dialog.confirm('您确认要删除此凭证吗？<br> 删除后将不可恢复，并会产生断号。', function() {
                $.ajax({
                    type: "POST",
                    url: "/gl/voucher?m=deleteById",
                    data: {"del": PAGE.vchID, "no": vchWordCombo.getText() + '-' + $('#vch_num').val()},
                    success: function(msg) {
                        if (PAGE.VIEW) {
                            parent.Public.tips({
                                content: '删除成功！',
                                top: 50
                            });
                            parent.tab.removeTabItem('voucherDetail');
                        } else {
                            parent.tab.reload("voucher", function() {
                                this.Public.tips({
                                    type: 0,
                                    content: '删除成功！'
                                });
                            });
                        }
                    }
                });
            });
        },
        print: function() {
            if (Voucher.curEditCell) {
                Voucher.cancelEdit(Voucher.curEditCell);
            }
            var voucherData = Voucher.getVoucherData();
            if (voucherData) {
                voucherData.id = PAGE.vchID;
                var pdfUrl = '/gl/voucher?m=printVoucher';
                $.dialog({
                    title: '凭证打印',
                    content: 'url:../print/print-settings-voucher.html?type=1',
                    data: {taodaData: {voucherIds: voucherData.id}, pdfData: {vchId: voucherData.id}, pdfUrl: pdfUrl},
                    width: 520,
                    height: 400,
                    min: false,
                    max: false,
                    lock: true,
                    ok: function() {
                        this.content.doPrint();
                        return false;
                    },
                    okVal: '打印',
                    cancel: true
                });
            }
        }
    }
};

thisPage.init();

$(window).load(function() {
    $("#save, #saveB").live('click', function(e) {
        e.preventDefault();
//		if (!Business.verifyRight('91')) {
//			return ;
//		}
        thisPage.handle.save();
    });

    $("#renew, #renewB").live('click', function(e) {
        e.preventDefault();
        if (!Business.verifyRight('91')) {
            return;
        }
        thisPage.handle.saveAndNew();
    });

    $("#add").live('click', function(e) {
        e.preventDefault();
        if (!Business.verifyRight('91')) {
            return;
        }
        thisPage.handle.add();
    });

    $("#addNew").live('click', function(e) {
        e.preventDefault();
        if (!Business.verifyRight('91')) {
            return;
        }
        var url = '/voucher/voucher.jsp?nd=' + new Date().getTime();
        parent.tab.addTabItem({tabid: 'voucher', text: '录凭证', url: url});
    });

    $("#edit, #editB").live('click', function(e) {
        e.preventDefault();
        if (!Business.verifyRight('92')) {
            return;
        }
        thisPage.handle.edit();
    });

    $("#copy").live('click', function(e) {
        e.preventDefault();
        PAGE.edit = false;
        thisPage.handle.copy();
    });

    $("#copyOld").live('click', function(e) {
        e.preventDefault();
        thisPage.handle.copyOld();

    });

    $("#delete").live('click', function(e) {
        e.preventDefault();
        if (!Business.verifyRight('94')) {
            return;
        }
        thisPage.handle.remove();
    });

    $("#print, #printB").live('click', function(e) {
        e.preventDefault();
        if (!Business.verifyRight('96')) {
            return;
        }
        thisPage.handle.print();
    });
});