var RelCompany = {
    add : function(id, name){
        $("#rel_company_id").val(id);
        $("#id_customer, #id_supplier").val(name);
        RelCompany.assign_linkman(id);
        $(".dismissModal").trigger("click");
    },
    assign_linkman : function(id) {
        $.get(_APP_ + "/CRM/RelationshipCompanyLinkman/ajax_getLinkman", {
            company_id: id
        }, function(data) {
            var _html = $("<select />");
            var _option;
            _html.addClass("span3");
            _html.attr("name", "customer_linkman_id");
            for (i = 0; i < data.length; i++) {
                _option = $("<option />");
                _option.attr("value", data[i].id)
                _option.text(data[i].contact);
                _html.append(_option);
            }
            $(".RelationshipCompanySelect").parent().find("select").remove();
            $(".RelationshipCompanySelect").after(_html);
        });
    },
    getCompanyDataTable : function(dataFilter) {
        $.get(_APP_+"/CRM/RelationshipCompany/ajax_getCompanyDataTable", function(data){
            $("#RelCompanySelectBox").html(data);
        })
    },
    BindEvent: function() {
        $(".RelationshipCompanySelect").autocomplete({
            source: _APP_ + "/CRM/RelationshipCompany/ajax_getCompanys",
            minLength: 1,
            select: function(event, ui) {
                $("#rel_company_id").val(ui.item.id);
                RelCompany.assign_linkman(ui.item.id);
            }
        });
    }
};