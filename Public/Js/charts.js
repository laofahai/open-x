var displayCharts = {
    saleDaysLine: function(container, days) {
        var title = sprintf("近%s天销售走势", days);
        $.get(_APP_ + "/Statistics/Sale/ajax_saleDaysLine", {
            days: days
        }, function(data) {
            displayCharts.displayLine(container, title, data.dateRange, data.series, "元");
        });

    },
    stockinDaysLine: function(container, days) {
        var title = sprintf("近%s天入库走势", days);
        $.get(_APP_ + "/Statistics/Stockin/ajax_stockinDaysLine", {
            days: days
        }, function(data) {
            displayCharts.displayLine(container, title, data.dateRange, data.series, "件");
        });

    },
    displayLine: function(container, title, categories, series, suffix) {
        suffix = suffix ? suffix : "";
        $(container).highcharts({
            title: {
                text: title,
                x: -20 //center
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: null
                },
                min: 0,
                plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
            },
            tooltip: {
                valueSuffix: suffix
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: series
        });
    },
    displayBar: function(container, title, categories, name, data, suffix, y_pre, total_pre) {
        var colors = Highcharts.getOptions().colors;
        y_pre = y_pre ? y_pre : "销售额: ";
        total_pre = total_pre ? total_pre : "订单数量: ";
        suffix = suffix ? suffix : " 元";
        $(container).highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: title
            },
            xAxis: {
                categories: categories,
                labels: {
                    rotation: -45,
                    align: 'right',
                    style: {
                        fontSize: '12px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                title: {
                    text: null
                }
            },
            plotOptions: {
                column: {
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: colors[0],
                        style: {
                            fontWeight: 'bold'
                        },
                        formatter: function() {
                            return this.y + suffix;
                        }
                    }
                }
            },
            tooltip: {
                formatter: function() {
                    var total = this.total ? this.total : 0;
                    var point = this.point,
                            s = this.x + y_pre + this.y + '</b><br/>';
                    s += total_pre + total;
                    return s;
                }
            },
            series: [{
                    name: name,
                    data: data,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        x: 4,
                        y: 10,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif',
                            textShadow: '0 0 3px black'
                        }
                    }
                }],
            exporting: {
                enabled: false
            }
        });
    }
};