var PageFrames = {
    Pages : [],
    Style : {
        width : 0,
        height: 0
    },
    Tpls : {
        tab : '<li tabid="%s" id="nav_tab_%s"><a>%s</a> <i class="icon icon-remove"></i> </li>',
        con : '<div tabid="%s" class="FrameItem" id="con_tab_%s"><iframe id="ctf_%s" src="%s" name="ctf_%s" border="0"></iframe></div>'
    },
    refresh : function() {
        var frameid = PageFrames.getActiveFrameId();
        window.frames[frameid].location.reload();
    },
    forward : function() {
        var frameid = PageFrames.getActiveFrameId();
        if(document.getElementById(frameid).contentWindow.history) {
            document.getElementById(frameid).contentWindow.history.forward();
        }
    },
    back : function() {
        var frameid = PageFrames.getActiveFrameId();
        if(document.getElementById(frameid).contentWindow.history) {
            document.getElementById(frameid).contentWindow.history.back();
        }
    },
    getActiveFrameId : function() {
        var tabid = $("#FramesContainer .FrameItem:visible").attr("tabid");
//        console.debug("ctf_"+tabid);
        return "ctf_"+tabid;
    },
    moveTo : function(obj) {
        var html;
        var tabid = $(obj).attr("tabid");
        if(!PageFrames.Pages.in_array(tabid)) {
            PageFrames.Pages.push(tabid);
            html = sprintf(PageFrames.Tpls.tab, tabid, tabid, $(obj).text());
            $("#FramesNavContainer ul").append(html);
            html = sprintf(PageFrames.Tpls.con, tabid, tabid, tabid, $(obj).attr("url"), tabid);
            $("#FramesContainer").append(html);
        }
        
        if(PageFrames.Pages.length > 10) {
            PageFrames.deletePage(PageFrames.Pages[1]);
        }
//        console.debug(PageFrames.Pages);
        var tabBtnId = $("#nav_tab_"+tabid);
        var tabConId = $("#con_tab_"+tabid);
        $("#FramesContainer .FrameItem").hide();
        tabConId.find("iframe").css({
            width: PageFrames.Style.width,
            height: PageFrames.Style.height
        });
        tabConId.show();
        tabBtnId.addClass("active").siblings().removeClass("active");
    },
    deletePage: function(tabid) {
        PageFrames.Pages.remove(tabid);
        $("#FramesNavContainer ul li[tabid='"+tabid+"']").prev().find("a").trigger("click");
        console.debug($("#FramesNavContainer ul li[tabid='"+tabid+"']").prev());
        $("#FramesNavContainer ul li[tabid='"+tabid+"']").remove();
        $("#FramesContainer [tabid='"+tabid+"']").remove();
    },
    bindEvent : function() {
        PageFrames.setWindowSize();
        $("#FramesNavContainer ul").delegate("li i", "click", function(){
            PageFrames.deletePage($(this).parent().attr("tabid"));
        });
        $("#FramesNavContainer ul").delegate("li a", "click", function(){
            PageFrames.moveTo($("#sidebar").find("a[tabid='"+$(this).parent().attr("tabid")+"']"));
        });
        $("#FramesNavContainer ul li i").remove();
    },
    setWindowSize : function() {
        PageFrames.Style.width = $("body").width()-$("#sidebar").width();
        PageFrames.Style.height= $("body").height()-$("#user-nav").height()-$("#footer").height()-20;
        $("#FramesContainer").find("iframe").css({
            width: PageFrames.Style.width,
            height:PageFrames.Style.height
        });
    }
};