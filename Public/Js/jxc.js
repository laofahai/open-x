var product = {
    stock_id : 0,
    search_by_code : function(keyword,mainrowid,callback) {
        console.debug(product.stock_id);
        if(!keyword) {
            return;
        }
        $.get(_APP_+"/JXC/Product/ajax_searchByCode",{
            keyword:keyword,
            stock_id : product.stock_id
        },function(data){
            product.search_callback(data, "#find_by_code_result", mainrowid, callback);
        });
    },
    search_by_name : function(keyword) {
        if(!keyword) {
            return;
        }
        $.get(_APP_+"/JXC/Product/ajax_searchByName",{
            keyword:keyword,
            stock_id : product.stock_id
        },function(data){
            product.search_callback(data, "#find_by_name_result");
        });
    },
    search_callback : function(data, container, mainrowid, callback) {
        $(container).html("");
            var _html;
            var _innerHTML;
            var _href;
            var theNum;
            if(data && data != "null") {
                for(i=0;i<data.length;i++) {
                    theNum = !data[i].num ? 0 : data[i].num;
                    _innerHTML = sprintf('%s / %s / %s / %s / %s', data[i].factory_code_all, data[i].name,data[i].color_name,data[i].standard_name,theNum);
                    _html = $("<a />").html(_innerHTML);
                    _html.attr("href", "#nogo");
                    _href = sprintf("javascript:%s(%s,'%s');", callback, mainrowid, data[i].factory_code_all);
                    _html.attr('onclick',_href);
//                    _html = $("<a />").html(data[i].factory_code_all+" "+data[i].name+"/"+data[i].color_name+"/"+data[i].standard_name);
//                    _html.attr("href", "javascript:"+callback+"("+mainrowid+","+data[i].id+","+data[i].color_id+","+data[i].standard_id+");");
                    $(container).append(_html);
                }
            }
    }
};

/**
 * 仓储部分入库/出库/订单 涉及订单操作
 **/
var paper = {
    addedList: [],
    add_product : function(mainrowid,factory_code_all) {
        if(paper.addedList.in_array(factory_code_all)) {
            return false;
        }
        paper.addedList.push(factory_code_all);
//        console.debug(paper.addedList);
        $.get(_APP_+"/JXC/"+_MODULE_+"/ajax_addProduct",{
            mainRowIdField: mainrowid,
            factory_code_all: factory_code_all
        }, function(data){
            if(!data) {
                alert("已添加");
                return;
            }
            if(data.message){
                alert(data.message);
            } else {
                $("#paper_table").append(data.html);
                paper.update_total_num();
                paper.update_total_price();
                paper.bind_event();
                $(".input_num:last").focus();
            }
        });
    },
    del_product: function(id,obj){
        if(confirm("{:L('confirm_delete')}")) {
            $.get(_APP_+"/JXC/"+_MODULE_+"/ajax_delProduct",{
                id:id
            },function(data){
                var factoryCodeAll = $("#paper_table tr[rowid="+id+"]").find("td:first").text();
//                console.debug(factoryCodeAll);
                paper.addedList.remove(factoryCodeAll);
//                console.debug(paper.addedList);
                if(data.message) {
                    alert(data.message);
                    return;
                } else {
                    $("#paper_table tr[rowid="+id+"]").fadeOut(function(){
                        $(this).remove();
                        paper.update_total_price();
                        paper.update_total_num();
                    });
                }
            });
        }
    },
    update_num: function(id,num){
        var ok = false;
        $.get(_APP_+"/JXC/"+_MODULE_+"/ajax_updateProductNum",{
            id: id,
            num:num
        }, function(data) {
            if(data.message) {
                alert(data.message);
                $("#paper_table tr[rowid="+id+"] input.input_num").focus();
            } else {
                ok = true;
                paper.update_price(id,num);
                paper.update_total_num();
                paper.update_total_price();
            }
        });
        
        return ok;
    },
    update_price : function(id,num) {
        var obj = $("#paper_table input.input_price[rowid="+id+"]");
        if(obj) {
            obj.val(Number(parseFloat($("#paper_table input.input_per_price[rowid="+id+"]").val()).toFixed(2)) * Number(num));
        }
    },
    update_perprice : function(id, perprice,num) {
        $.get(_APP_+"/JXC/"+_MODULE_+"/ajax_updatePerPrice",{
            id: id,
            price: perprice
        }, function(){
            paper.update_price(id,num);
            paper.update_total_price();
        });
    },
    update_price_remote : function(id,price) {
        $.get(_APP_+"/JXC/"+_MODULE_+"/ajax_updatePrice",{
            id: id,
            price: price
        }, function(){
            paper.update_total_price();
        });
    },
    
    update_total_num: function(){
        var sum = 0;
        $("#paper_table .input_num").each(function(){
            sum += parseInt($(this).val());
        });
        $("#total_num").text(sum);
    },
    update_total_price : function() {
        
        var sum = Number(0);
        $("#paper_table .input_price").each(function(){
            sum += Number(parseFloat($(this).val()).toFixed(2));
        });
        if($("#total_price")) {
            $("#total_price").text(sum);
        }
        if($("#total_price_real")) {
            $("#total_price_real").val(sum);
        }
    },
    paper_submit: function(){
        $("#total_price_hide").val($("#total_price_real").val());
        $("#willsave").val(1).parent().submit();
    },
    bind_event: function(){
        $("#paper_table .input_num").keydown(function(e){
            if(e.which == 13 || e.keyCode == 13) {
                var nextInput = $(this).parent().next().next().find("input");
                if(nextInput.className == "the_num") {
                    nextInput.focus();
                } else {
                    $("#find_by_code").focus();
                }
            }
        }).blur(function(){
            if(paper.update_num($(this).attr("rowid"), $(this).val())) {
                paper.update_price($(this).attr("rowid"), $(this).val());
            }
        });
        
        $("#paper_table .input_per_price").keydown(function(e){
            if(e.which == 13 || e.keyCode == 13) {
                //paper.stockin_update_num($(this).attr("rowid"), $(this).val());
                $(this).parent().next().find("input").focus();
            }
        }).blur(function(){
            
            paper.update_perprice($(this).attr("rowid"), $(this).val(),$(this).parent().next().find("input").val());
        });
        
        $("#find_by_code").keydown(function(e){
            if(e.which == 13 || e.keyCode == 13) {
                $("#find_by_code_result a:first").trigger("click");
            }
        });
        
        if($("#paper_table .input_price")) {
           $("#paper_table .input_price").keyup(function(e){
               if(e.which == 13 || e.keyCode == 13) {
                    //paper.stockin_update_num($(this).attr("rowid"), $(this).val());
                    $("#find_by_code").focus();
                }
           }).blur(function(){
               //paper.update_price($(this).attr("rowid"), $(this).val());
               paper.update_price_remote($(this).attr("rowid"), $(this).val());
           }); 
        }
    }
};